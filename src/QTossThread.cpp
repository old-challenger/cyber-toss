#include <QTossThread.h>
#include <globals.h>

#include <cstdlib>
#include <ctime>
#include <set>

void QTossThread::run () {
   if ( !notStopFlag_m ) 
      return ;
   const auto  currentRoundId_cl = global::data::champ().rounds().empty() 
      ? 0 : global::data::champ().currentRound() + 1;
   // ���������� � ���������� ���
   if ( global::data::champ().rounds().empty() ) 
      global::data::champ().reserveRounds();
   else {
      assert( global::data::champ().rounds().empty() || global::data::champ().roundIsFinished() );;
   }
   //   
   const auto  & round_crl = global::data::champ().rounds().at( currentRoundId_cl );
   const auto  & roundPlayers_crl = round_crl.players();
   const auto  playersAmount_cl = round_crl.playersAmount(),
               gamesAmount_cl = playersAmount_cl / 2;          //< ���������� ��� � ��������� ����
   assert( 
      ( ( 0 == currentRoundId_cl && playersAmount_cl < round_crl.denominator() * 2 )
         || playersAmount_cl == round_crl.denominator() * 2 ) && !( playersAmount_cl % 2 ) );
   emit tossStarted( round_crl.denominator() );
   // ��������� ��� ������ ������� � ��������� ����
   auto  favorites_it = roundPlayers_crl.cbegin(),
         outsiders_it = roundPlayers_crl.cend();
   --outsiders_it;
   // ��������� ��� ���������� �������� ���, �������������� �����������
   championship::round::games_vector games_l;   
   games_l.reserve( round_crl.denominator() );
   // ��������� ����������� �������
   maped_sets chosenPlayers_l;                  
   maped_sets::const_iterator insertPosition_l = chosenPlayers_l.constEnd();
   // ������� ������ ����� �� ��������� ����������� ������� ( ���������� ������� ��������� )
   for ( auto it = global::data::champ().players().constBegin(); 
         global::data::champ().players().constEnd() != it; ++it )
      insertPosition_l = chosenPlayers_l.insert( insertPosition_l, it.key(), championship::ids_set() );
   //   
   int favoritePlayer_l, favoriteNation_l, outsiderPlayer_l, outsiderNation_l;
   srand( time( NULL ) );
   // �������� ����������� ������� � �������
   championship::size_type favoritesCounter_l = favorites_it.value().size(),
                           outsidersCounter_l = outsiders_it.value().size();
   for ( int i = 0; notStopFlag_m && gamesAmount_cl > i; --favoritesCounter_l, --outsidersCounter_l ) {
      emit nextGame ( ++i );
      while ( !favoritesCounter_l )
         favoritesCounter_l = ( ++favorites_it ).value().size();
      while ( !outsidersCounter_l )
         outsidersCounter_l = ( --outsiders_it ).value().size();
      favoritePlayer_l = chooseNextPlayerId( FAVORITE_PLAYERS_NAME, favorites_it, chosenPlayers_l ); 
      favoriteNation_l = chooseNextNationId( FAVORITE_PLAYERS_NATION ); 
      outsiderPlayer_l = chooseNextPlayerId( OUTSIDER_PLAYERS_NAME, outsiders_it, chosenPlayers_l ); 
      outsiderNation_l = chooseNextNationId( OUTSIDER_PLAYERS_NATION );
      games_l.push_back( championship::round::game(  
         championship::round::player_pair( favorites_it.key(), favoritePlayer_l ), favoriteNation_l,
         championship::round::player_pair( outsiders_it.key(), outsiderPlayer_l ), outsiderNation_l ) );
      if ( notStopFlag_m )
         msleep( nextGameDelay_scm );
   } // for ( int i = 0; !stopFlag_m && i < gamesAmount_l; ) {
   //
   if ( notStopFlag_m ) {
      if ( currentRoundId_cl )
         global::data::champ().nextRound();
      global::data::champ().clearRoundPlayers();
      global::data::champ().addGames( games_l );
      emit championshipDataUpdated();
   }
}

int QTossThread::chooseNextNationId ( QTossThread::textValueTypes_enum textValueType_p ) {
   assert( FAVORITE_PLAYERS_NATION == textValueType_p || OUTSIDER_PLAYERS_NATION == textValueType_p );
   championship::size_type choosenNationId_l = global::data::champ().nations().front();
   for ( auto i = tossIterationsAmount_scm; notStopFlag_m && i; --i ) {
      emit nextTxtValue ( textValueType_p, QString() );
      choosenNationId_l = global::data::champ().nations().at( rand() % global::data::champ().nations().size() );
      msleep( cleansFieldDelay_scm );
      emit nextTxtValue ( textValueType_p, global::data::champ().nationsNames().value( choosenNationId_l ) );
      msleep( fillFiledDelay_scm );
      if ( global::data::champ().nations().size() < 2 )
         break;
   }
   return choosenNationId_l;
}

int QTossThread::chooseNextPlayerId ( QTossThread::textValueTypes_enum textValueType_p, 
      const championship::round::roundsPlayers_map::const_iterator & playersGroup_cp, 
      maped_sets & chosenPlayers_rp ) {
   assert( playersGroup_cp.value().size() );
   assert( FAVORITE_PLAYERS_NAME == textValueType_p || OUTSIDER_PLAYERS_NAME == textValueType_p );
   championship::size_type choosenPlayerId_l = playersGroup_cp.value().front();
   auto chosenPlayersIn1Group_l = chosenPlayers_rp.find( playersGroup_cp.key() );
   assert( chosenPlayers_rp.end() != chosenPlayersIn1Group_l );
   if ( 1 == playersGroup_cp.value().size() ) {
      const auto & player_crl = global::data::champ().players().find( playersGroup_cp.key() ).value().find( 
         choosenPlayerId_l ).value();
      emit nextTxtValue ( textValueType_p, global::consts::txt::playerComplexName( player_crl.clan(), 
         player_crl.nickname() ) );
   } else for ( auto i = tossIterationsAmount_scm; notStopFlag_m && i; ) {
      emit nextTxtValue ( textValueType_p, QString() );
      if( chosenPlayersIn1Group_l.value().end() == chosenPlayersIn1Group_l.value().find( 
            choosenPlayerId_l = playersGroup_cp.value().at( rand() % playersGroup_cp.value().size() ) ) )
         --i;
      msleep( cleansFieldDelay_scm );
      const auto & player_crl = global::data::champ().players().find( playersGroup_cp.key() ).value().find( 
         choosenPlayerId_l ).value();
      emit nextTxtValue ( textValueType_p, global::consts::txt::playerComplexName( player_crl.clan(), 
         player_crl.nickname() ) );
      msleep( fillFiledDelay_scm );
   }
   assert( chosenPlayersIn1Group_l.value().end() == chosenPlayersIn1Group_l.value().find( choosenPlayerId_l ) );
   chosenPlayersIn1Group_l.value().insert( choosenPlayerId_l );
   return choosenPlayerId_l;
}


