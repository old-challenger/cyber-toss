#include <QNationsListModel.h>
#include <globals.h>

#include <cassert>

std::vector< QString > QNationsListModel::createHeaderVector () {
   std::vector< QString > oneStringVector_l;
   oneStringVector_l.push_back( global::consts::txt::tr( "�������� �����" ) );
   return oneStringVector_l;
}

void QNationsListModel::add ( int nationId_p ) {
   global::data::champ().addNation( nationId_p );
   assert( global::data::champ().nations().size() );
   const int updatingIndex_cl = global::data::champ().nations().size() - 1;
   emit dataChanged( createIndex( updatingIndex_cl, 0 ), createIndex( updatingIndex_cl, 0 ) );
}

QModelIndex QNationsListModel::index ( int row_p, int column_p, const QModelIndex & /*parent_cp*/ ) const {
   return row_p < global::data::champ().nations().size() ? createIndex( row_p, column_p ) : QModelIndex();      
}

QVariant QNationsListModel::data ( const QModelIndex & index_cp, int role_p ) const {
   if ( !index_cp.isValid() )
      return QVariant();
   switch ( role_p ) {
      case Qt::DisplayRole: {
         const championship::ids_vector & nations_cl = global::data::champ().nations();
         assert( index_cp.row() < nations_cl.size() );
         assert( !index_cp.parent().isValid() );
         assert( global::data::champ().nationsNames().end() != global::data::champ().nationsNames().find( 
            nations_cl.at( index_cp.row() ) ) );
         return global::data::champ().nationsNames()[ global::data::champ().nations().at( index_cp.row() ) ];
      }
      default: return QVariant();
   }
   return QVariant();
}

void QNationsListModel::remove( const QModelIndexList & indexes_cp ) {
   championship::ids_set idsForDelete_l;
   for ( QModelIndexList::const_iterator it = indexes_cp.constBegin(); indexes_cp.constEnd() != it; ++it ) 
      idsForDelete_l.insert( global::data::champ().nations().at( static_cast< std::size_t >( it->row() ) ) );
   global::data::champ().deleteNations( idsForDelete_l );
   emit dataChanged( createIndex( 0, 0 ), 
      createIndex( static_cast< int >( global::data::champ().nations().size() - 1 ), 0 ) );
}

int QNationsListModel::rowCount( const QModelIndex & /*parent_cp*/ ) const { 
   return static_cast< int >( global::data::champ().nations().size() ); 
}
