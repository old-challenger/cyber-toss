#include <QDiagramScene.h>
#include <QGameWidget.h>
#include <globals.h>

#include <QFontMetricsF>
#include <QLabel>
#include <cassert>

QDiagramScene::roundItems_vector::size_type QDiagramScene::addAndPoseWidget ( championship::size_type roundSectionId_p, 
      championship::size_type widgetIndex_p, QWidget * widget_p, roundItems_vector & itemsContainer_rp, bool player_p ) {
   const auto result_cl = itemsContainer_rp.size();
   itemsContainer_rp.push_back( addWidget( widget_p ) );
   const auto gameSectionWigth_cl = roundSectionSize_m.width() / ( player_p ? 2 : 1 ) / 
      global::data::champ().rounds().at( roundSectionId_p ).denominator();
   itemsContainer_rp.back()->graphicsItem()->setPos( QPointF( 
      gameSectionWigth_cl * widgetIndex_p + ( gameSectionWigth_cl - widget_p->width() ) / 2, 
      roundSectionY( roundSectionId_p ) + roundTitleHeight_m + ( roundSectionSize_m.height() - widget_p->height() ) / 2 ) );
   return result_cl;
}

void QDiagramScene::clearDiagram () {
   for ( auto roundGames_it : gamesItems_m )
      clearGraphWidgtesVector( roundGames_it );
   gamesItems_m.clear();
   clearGraphItemsVector( roundsField_m );
   clearGraphItemsVector( roundsTitles_m );
   removePlayersItems();
}

void QDiagramScene::checkMaxTextWidth ( const QString & text_cp, const QFontMetricsF & metrics_cp, 
                                          QSizeF & currentMaxSize_rp ) {
   const qreal width_cl = metrics_cp.width( text_cp ); 
   if ( width_cl > currentMaxSize_rp.width() ) {
      currentMaxSize_rp.setWidth( width_cl );
      maxLengthString_m = text_cp;
   }
}

QWidget * QDiagramScene::createPlayerWidget ( championship::size_type groupId_p, 
      championship::size_type playerId_p, const QString & maxLineText_cp ) {
   const auto & player_crl = global::data::champ().players().value( groupId_p ).value( playerId_p );
   QLabel * const result_l = new QLabel( global::consts::txt::playerComplexName( player_crl.clan(), 
      player_crl.nickname() ) );
   result_l->setAlignment( Qt::AlignCenter );
   const QFontMetrics metrics_cl( result_l->font() );
   result_l->setMinimumWidth( metrics_cl.width( maxLineText_cp ) );
   result_l->setMinimumHeight( metrics_cl.height() * 2 );
   return result_l;
}

void QDiagramScene::initDiagram( championship::size_type roundsAmount_p ) {
   gamesItems_m.resize( roundsAmount_p );
   // ���������� ���������� �����
   // ������ ������������ �������� �������� �� �����
   const QFontMetricsF metrics_cl( font() );          
   /* ������ ���� ��������� ����� (������ ���� �������� ����� �����)
      ����������� ������� ������ (�������� ����, ��� ������ � ������ ��� �����)
   */
   const auto & firstRound_crl = global::data::champ().rounds().front();
   QSizeF maxWidgetSize_l = QSizeF( metrics_cl.width( maxLengthString_m = 
      global::consts::txt::gameComplexName( firstRound_crl.denominator(), firstRound_crl.games().size() ) ), 
      metrics_cl.height() * 6 );           
   // �������� ���� ���� �������
   for ( const auto playersGoup_it : global::data::champ().players() )
      for ( const auto player_it : playersGoup_it ) 
         checkMaxTextWidth( global::consts::txt::playerComplexName( player_it.clan(), player_it.nickname() ),
            metrics_cl, maxWidgetSize_l );
   // �������� ���� �������� �����
   for ( const auto nation_it : global::data::champ().nationsNames() ) 
      checkMaxTextWidth( global::consts::txt::nationTemplate().arg( nation_it ), metrics_cl, maxWidgetSize_l );
   /* ������ ������ (������� �� �����), � ������� ����� ������������ ������ ������ ���������� 
      ������� �������.
   */
   static const qreal textMargin_scl = 1.1;
   roundTitleHeight_m = metrics_cl.height() * textMargin_scl;
   roundSectionSize_m.setWidth( maxWidgetSize_l.width() * 1.5 * global::data::champ().rounds().front().denominator() );
   roundSectionSize_m.setHeight( maxWidgetSize_l.height() * 2.5 + roundTitleHeight_m );
   // �������� ����� ��� ����������� �������
   const QBrush backgroundBrush_cl( QColor( 227, 147, 147 ) );
   roundsField_m.reserve( roundsAmount_p );
   QGraphicsTextItem * title_l;
   for ( auto i = roundsAmount_p; i; ) {
      roundsField_m.push_back(
         addRect( 0, roundSectionY( --i ), roundSectionSize_m.width(), roundSectionSize_m.height(), QPen(), 
            backgroundBrush_cl ) );
      roundsTitles_m.push_back( title_l = addText( global::consts::txt::roundsName( 
         global::data::champ().rounds().at( i ).denominator() ) ) );
      title_l->setPos( roundSectionSize_m.width() / 2 - title_l->boundingRect().width() / 2,
         roundSectionY( i ) );
   }
}

void QDiagramScene::removePlayersItems() {
   clearGraphWidgtesVector( playersItems_m );
   gamesResultsIndex_m.clear();
}

void QDiagramScene::updateDiagram() {
   const auto roundsAmount_cl = global::data::champ().rounds().size();
   assert( roundsAmount_cl );
   // ������ ������
   if ( roundsAmount_cl != gamesItems_m.size() ) 
      initDiagram( roundsAmount_cl );
   // �������� �� ����� ��������� � ������� ������� ���������� ������
   else if ( playersItems_m.size() )
      removePlayersItems();
   const auto & rounds_cl = global::data::champ().rounds(); 
   //QWidget * newWdgt_l = nullptr;
   championship::size_type gameSectionIndex_l = 0; 
   QMap< championship::size_type, championship::ids_set > connectedPlayers_l;
   for ( auto group_it = global::data::champ().players().constBegin();
      global::data::champ().players().end() != group_it; ++ group_it )
      connectedPlayers_l.insert( connectedPlayers_l.end(), group_it.key(), championship::ids_set() );
   for ( championship::size_type i = 0; i < roundsAmount_cl; ++i ) {
      if ( rounds_cl.at( i ).games().size() ) {
         if ( rounds_cl.at( i ).games().size() == gamesItems_m[ i ].size() )
            continue;
         const auto gamesAmount_cl = rounds_cl.at( i ).games().size();
         gameSectionIndex_l = 0;
         for ( ; gameSectionIndex_l < gamesAmount_cl; ++gameSectionIndex_l ) {
            addAndPoseWidget( i, gameSectionIndex_l, 
               new QGameWidget( i, gameSectionIndex_l, maxLengthString_m ), gamesItems_m[ i ] );
            const auto & addingGame_cl = global::data::champ().rounds().at( i ).games().at( gameSectionIndex_l );
            if ( global::data::champ().currentRound() == i
                  && championship::round::game::UNKNOWN != addingGame_cl.result() ) {
               updateGameResult( gameSectionIndex_l );
               const auto & gameWinner_cl = addingGame_cl.winner();
               connectedPlayers_l[ gameWinner_cl.groupId() ].insert( gameWinner_cl.playerId() );
            }
         }
      } else {
         assert( !gamesItems_m.at( i ).size() );
         if ( !rounds_cl.at( i ).players().size() )
            break;
         for ( auto group_it = rounds_cl.at( i ).players().cbegin(); 
               rounds_cl.at( i ).players().cend() != group_it; ++group_it )
            for ( auto player_it : group_it.value() ) { 
               const auto & connectedGroup_cl = connectedPlayers_l.value( group_it.key() );
               if ( connectedGroup_cl.constEnd() == connectedGroup_cl.find( player_it ) )
                  addAndPoseWidget( i, gameSectionIndex_l++, createPlayerWidget( group_it.key(), player_it, 
                     maxLengthString_m ), playersItems_m, true );
            }
         break; // ������ ������������ � ��������� �� ������ ������ (��������� ��� �� ���������)
      } // if ( rounds_cl.at( i ).games().size() ) {
   } // for ( championship::size_type i = 0; i < roundsAmount_cl; ++i ) {
}

void QDiagramScene::updateGameResult ( championship::size_type gameId_p ) {
   const auto currentRound_cl = global::data::champ().currentRound();
   if ( currentRound_cl == global::data::champ().rounds().size() - 1 )
      return ;
   const auto & game_cl = global::data::champ().rounds().at( currentRound_cl ).games().at( gameId_p );
   const auto & winner_cl = game_cl.winner();
   auto found_l = gamesResultsIndex_m.lowerBound( gameId_p );
   if ( gamesResultsIndex_m.end() == found_l || found_l.key() != gameId_p ) {
      QWidget * const newPlayerWidget_l = createPlayerWidget( winner_cl.groupId(), winner_cl.playerId(), maxLengthString_m );
      const auto playersItemId_cl = addAndPoseWidget( currentRound_cl + 1, gameId_p, newPlayerWidget_l, playersItems_m, true );
      found_l = gamesResultsIndex_m.insert( found_l, gameId_p, playersItemId_cl );
   } else {
      QLabel * const foundWidget_l = dynamic_cast< QLabel * >( playersItems_m.at( found_l.value() )->widget() );
      assert( nullptr != foundWidget_l );
      const auto & player_crl = global::data::champ().players().value( winner_cl.groupId() ).value( winner_cl.playerId() );
      foundWidget_l->setText( global::consts::txt::playerComplexName( player_crl.clan(), 
         player_crl.nickname() ) );
   }
}