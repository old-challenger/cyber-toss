#include <QStringEnterDialog.h>
#include <globals.h>

#include <QLabel>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QPushButton>

QStringEnterDialog::QStringEnterDialog ( const std::vector< QString > & fieldsHeaders_cp ) : QDialog( nullptr, 
      Qt::CustomizeWindowHint | Qt::WindowTitleHint ), stringFields_m( fieldsHeaders_cp.size() ) {
   QVBoxLayout * const vLayout_l = new QVBoxLayout( this );
      QGridLayout * const gridLayout_l = new QGridLayout( this );
      vLayout_l->addLayout( gridLayout_l );
      for( std::size_t i = 0; i < fieldsHeaders_cp.size(); ++i ) {
         stringFields_m.at( i ) = new QLineEdit( this );
         gridLayout_l->addWidget( new QLabel( fieldsHeaders_cp.at( i ), this ), static_cast< int >( i ), 0 );
         gridLayout_l->addWidget( stringFields_m.at( i ), static_cast< int >( i ), 1 );
      }
      QHBoxLayout * const hLayout_l = new QHBoxLayout();
      vLayout_l->addLayout( hLayout_l );
         hLayout_l->addStretch( 1 );
         QPushButton * const okBtn_l = new QPushButton( global::consts::icons::check(), 
            global::consts::txt::tr( "������ �������" ), this ),
                     * const cancelBtn_l  = new QPushButton( global::consts::icons::cross(), 
            global::consts::txt::cancel(), this );
         hLayout_l->addWidget( okBtn_l );
         hLayout_l->addWidget( cancelBtn_l );
   connect( okBtn_l,       SIGNAL( released() ), this, SLOT( accept() ) );
   connect( cancelBtn_l,   SIGNAL( released() ), this, SLOT( reject() ) );
}

int QStringEnterDialog::exec() {
   for( auto i = stringFields_m.size(); 0 < i; )
      stringFields_m.at( --i )->setText( QString() );
   stringFields_m.front()->setFocus();
   return QDialog::exec();
}


