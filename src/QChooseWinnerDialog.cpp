#include <globals.h>
#include <QChooseWinnerDialog.h>

#include <QRadioButton>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>

#include <cassert>

QChooseWinnerDialog::QChooseWinnerDialog() : QDialog( nullptr, Qt::CustomizeWindowHint | Qt::WindowTitleHint ), 
   gameLbl_m( nullptr ), okBtn_m( nullptr ), favorite_m( nullptr ), outsider_m( nullptr ) {
   QVBoxLayout * const vLayout_l = new QVBoxLayout( this );
      vLayout_l->addWidget( gameLbl_m = new QLabel( this ) );
      QGroupBox * const radioGroup_l = new QGroupBox( global::consts::txt::tr( "�������� ������-����������" ), this );
      vLayout_l->addWidget( radioGroup_l );
         QVBoxLayout * const radioLayout_l = new QVBoxLayout( radioGroup_l );
            radioLayout_l->addWidget( favorite_m = new QRadioButton( radioGroup_l ) );
            radioLayout_l->addWidget( outsider_m = new QRadioButton( radioGroup_l ) );
      QHBoxLayout * const buttonsLayout_l = new QHBoxLayout();
      vLayout_l->addLayout( buttonsLayout_l );
         buttonsLayout_l->addStretch( 1 );
         buttonsLayout_l->addWidget( okBtn_m = new QPushButton( global::consts::icons::check(), 
                                       global::consts::txt::check(), this ) );
            okBtn_m->setEnabled( false );
         QPushButton * const cancelBtn_l = new QPushButton( global::consts::icons::cross(), 
            global::consts::txt::cancel(), this );
         buttonsLayout_l->addWidget( cancelBtn_l );
   setWindowTitle( global::consts::txt::tr( "����� ����������" ) );
   setWindowIcon( global::consts::icons::toss() );
   //
   connect( okBtn_m,       SIGNAL( released() ),   this, SLOT( accept() ) );
   connect( cancelBtn_l,   SIGNAL( released() ),   this, SLOT( reject() ) );
   connect( favorite_m,    SIGNAL( pressed() ),    this, SLOT( slotActivateOkBtn() ) );
   connect( outsider_m,    SIGNAL( pressed() ),    this, SLOT( slotActivateOkBtn() ) );
}

championship::round::game::result_enum QChooseWinnerDialog::execChoosing () {
   if ( QDialog::Rejected == QDialog::exec() )
      return championship::round::game::UNKNOWN;
   if ( favorite_m->isChecked() )
      return championship::round::game::EXPECTED;
   if ( outsider_m->isChecked() )
      return championship::round::game::UNEXPECTED;
   assert( !"[ERROR] Incorrect IF condition." );
   return championship::round::game::UNKNOWN;
}

void QChooseWinnerDialog::setFavorite( const QString & player_cp, const QString & nation_cp ) {
   favorite_m->setText( playerPlusNation( player_cp, nation_cp ) );
}

void QChooseWinnerDialog::setGame( const QString & gameFullName_cp ) {
   gameLbl_m->setText( gameFullName_cp );
}

void QChooseWinnerDialog::setGameResult ( championship::round::game::result_enum result_p ) {
   const bool  favorite_cl = championship::round::game::EXPECTED == result_p,
               outsider_cl = championship::round::game::UNEXPECTED == result_p;
   favorite_m->setChecked( favorite_cl );
   outsider_m->setChecked( outsider_cl );
   if ( !okBtn_m->isEnabled() && ( favorite_cl || outsider_cl ) )
      okBtn_m->setEnabled( true );
}  

void QChooseWinnerDialog::setOutsider( const QString & player_cp, const QString & nation_cp ) {
   outsider_m->setText( playerPlusNation( player_cp, nation_cp ) );
}

void QChooseWinnerDialog::slotActivateOkBtn () {
   okBtn_m->setEnabled( true );
}
