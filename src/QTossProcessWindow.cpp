#include <QTossProcessWindow.h>
#include <globals.h>

#include <QVBoxLayout>
#include <QPushButton>

QTossProcessWindow::QTossProcessWindow () : QFrame( nullptr, Qt::CustomizeWindowHint | Qt::WindowTitleHint ), 
      gameNumber_m( nullptr ), favoritePlayer_m( nullptr ), favoriteNation_m( nullptr ), 
      outsiderPlayer_m( nullptr ), outsiderNation_m( nullptr ) {
   QFont gameFont_l     = font(),
         playerFont_l   = font(),
         nationFont_l   = font();
      gameFont_l.setBold( true );
   //
   QVBoxLayout * const vLayout_l = new QVBoxLayout( this );
   vLayout_l->addWidget( gameNumber_m = new QLabel( this ) );
      gameNumber_m->setFont( gameFont_l );
   vLayout_l->addWidget( favoritePlayer_m = new QLabel( this ) );
      favoritePlayer_m->setFont( playerFont_l );
      favoritePlayer_m->setAlignment( Qt::AlignCenter );
   vLayout_l->addWidget( favoriteNation_m = new QLabel( this ) );
      favoriteNation_m->setFont( nationFont_l );
      favoriteNation_m->setAlignment( Qt::AlignCenter );
   QLabel * const vsLabel_l = new QLabel( QLatin1String( "VS" ), this );
      vsLabel_l->setAlignment( Qt::AlignCenter );
   vLayout_l->addWidget( vsLabel_l );
   vLayout_l->addWidget( outsiderPlayer_m = new QLabel( this ) );
      outsiderPlayer_m->setFont( playerFont_l );
      outsiderPlayer_m->setAlignment( Qt::AlignCenter );
   vLayout_l->addWidget( outsiderNation_m = new QLabel( this ) );
      outsiderNation_m->setFont( nationFont_l );
      outsiderNation_m->setAlignment( Qt::AlignCenter );
   QHBoxLayout * const hLayout_l = new QHBoxLayout();
   vLayout_l->addLayout( hLayout_l );
      hLayout_l->addStretch( 1 );
      QPushButton * const close_l = new QPushButton( global::consts::icons::cross(), 
         global::consts::txt::cancel(), this );
      hLayout_l->addWidget( close_l );
      connect( close_l, SIGNAL( released() ), global::objects::tossThread(), SLOT( sltStop() ) );
   //
   setWindowIcon( global::consts::icons::toss() );
}

void QTossProcessWindow::sltClean () {
   favoritePlayer_m->setText( QString() );
   favoriteNation_m->setText( QString() );
   outsiderPlayer_m->setText( QString() );
   outsiderNation_m->setText( QString() );
}

void QTossProcessWindow::sltSetGameNumber ( int number_p ) {
   static const QString text_scl = global::consts::txt::tr( "���� %1" ); 
   assert( number_p &&
      global::data::champ().rounds().at( global::data::champ().currentRound() ).denominator() >= number_p );
   gameNumber_m->setText( text_scl.arg( QString::number( number_p ) ) );
}

void QTossProcessWindow::sltSetTxtValue ( int textValueType_p, QString textValue_p ) {
   switch ( textValueType_p ) {
      case QTossThread::FAVORITE_PLAYERS_NAME: 
         favoritePlayer_m->setText( textValue_p );
      break;
      case QTossThread::FAVORITE_PLAYERS_NATION:
         favoriteNation_m->setText( textValue_p.isEmpty() ? textValue_p 
            : global::consts::txt::nationTemplate().arg( textValue_p ) );
      break;
      case QTossThread::OUTSIDER_PLAYERS_NAME:  
         outsiderPlayer_m->setText( textValue_p );
      break;
      case QTossThread::OUTSIDER_PLAYERS_NATION:
         outsiderNation_m->setText( textValue_p.isEmpty() ? textValue_p 
            : global::consts::txt::nationTemplate().arg( textValue_p ) );
      break;
      default:
         assert( !"[ERROR] Incorrect case path" );
   }
}

void QTossProcessWindow::sltSetWindowTitle( int tourDenominator_p ) {
   assert( tourDenominator_p > 0 && ( 1 == tourDenominator_p || !( tourDenominator_p % 2 ) ) );
   static const QString title_scl      = global::consts::txt::tr( "���������� %1������ ����������" ),
      half_scl       = global::consts::txt::tr( "����" ),
      oneForth_scl   = global::consts::txt::tr( "��������" ),
      enumerator_scl = QLatin1String( "1/" );
   //
   QFrame::setWindowTitle( tourDenominator_p > 4 
      ? title_scl.arg( enumerator_scl + QString::number( tourDenominator_p ) + global::consts::txt::space() ) 
      : ( 4 == tourDenominator_p ? title_scl.arg( oneForth_scl ) 
      : ( 2 == tourDenominator_p ? title_scl.arg( half_scl ) : title_scl.arg( QString() ) ) ) );
}
