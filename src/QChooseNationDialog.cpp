#include <QChooseNationDialog.h>
#include <globals.h>

#include <QVBoxLayout>

#include <set>

QChooseNationDialog::QChooseNationDialog () : QDialog( nullptr, 
      Qt::CustomizeWindowHint | Qt::WindowTitleHint ), comboBox_m( nullptr ) {
   QVBoxLayout * const vLayout_l = new QVBoxLayout( this );
   vLayout_l->addWidget( comboBox_m = new QComboBox( this ) );
      QHBoxLayout * const hLayout_l = new QHBoxLayout();
      vLayout_l->addLayout( hLayout_l );
      hLayout_l->addStretch( 1 );
      QPushButton * const okBtn_l = new QPushButton( global::consts::icons::check(), 
         global::consts::txt::tr( "������ �������" ), this ),
                  * const cancelBtn_l  = new QPushButton( global::consts::icons::cross(), 
         global::consts::txt::cancel(), this );
      hLayout_l->addWidget( okBtn_l );
      hLayout_l->addWidget( cancelBtn_l );
      connect( okBtn_l,       SIGNAL( released() ), this, SLOT( accept() ) );
      connect( cancelBtn_l,   SIGNAL( released() ), this, SLOT( reject() ) );
}

int QChooseNationDialog::exec() {
   std::set< championship::size_type > addedNations_l;
   for ( auto nation_id : global::data::champ().nations() )
      addedNations_l.insert( nation_id );
   comboBox_m->clear();
   for ( auto nation_it = global::data::champ().nationsNames().cbegin(); 
         global::data::champ().nationsNames().cend() != nation_it; ++nation_it )
      if ( addedNations_l.end() == addedNations_l.find( nation_it.key() ) )
         comboBox_m->addItem( nation_it.value(), QVariant::fromValue( nation_it.key() ) );
   comboBox_m->setFocus();
   return QDialog::exec();
}

