#include <QGameWidget.h>
#include <globals.h>

#include <QFontMetrics>
#include <QVBoxLayout>
#include <QMouseEvent>
#include <QString>
#include <QLabel>
#include <QMap>

QGameWidget::QGameWidget( championship::size_type roundId_p, championship::size_type gameId_p, 
      const QString & maxLineText_cp ) : QFrame( nullptr ), roundId_m( roundId_p ), gameId_m( gameId_p ), 
      game_m( nullptr ), favoriteName_m( nullptr ), favoriteNation_m( nullptr ), outsiderName_m( nullptr ), 
      outsiderNation_m( nullptr ) {
   const auto  & round_cl = global::data::champ().rounds().at( roundId_p );
   const auto  & game_cl = round_cl.games().at( gameId_p );
   const auto  & favorite_crl = global::data::champ().players().value( game_cl.favorite().groupId() ).value( 
                  game_cl.favorite().playerId() ),
               & outsider_scl = global::data::champ().players().value( game_cl.outsider().groupId() ).value( 
                  game_cl.outsider().playerId() );
   QVBoxLayout * const layout_l = new QVBoxLayout( this );
      layout_l->addWidget( game_m = createLabel( global::consts::txt::gameComplexName( 
         round_cl.denominator(), gameId_p ) ) );
      layout_l->addWidget( favoriteName_m = createLabel( global::consts::txt::playerComplexName( 
         favorite_crl.clan(), favorite_crl.nickname() ) ) );
      layout_l->addWidget( favoriteNation_m = createLabel( global::consts::txt::nationTemplate().arg( 
         global::data::champ().nationsNames().value( game_cl.favoriteNation() ) ) ) );
      layout_l->addWidget( createLabel( "VS" ) );
      layout_l->addWidget( outsiderName_m = createLabel( global::consts::txt::playerComplexName( 
         outsider_scl.clan(), outsider_scl.nickname() ) ) );
      layout_l->addWidget( outsiderNation_m = createLabel( global::consts::txt::nationTemplate().arg(
         global::data::champ().nationsNames().value( game_cl.outsiderNation() ) ) ) );
   setFrameStyle( QFrame::Shape::NoFrame | QFrame::Shadow::Plain );
   const QFontMetrics metrics_cl( font() );
   setMinimumWidth( metrics_cl.width( maxLineText_cp ) + layout_l->margin() * 2 );
   //
}

QLabel * QGameWidget::createLabel ( const QString & text_cl ) {
   QLabel * const result_l = new QLabel( text_cl, this );
   result_l->setAlignment( Qt::AlignCenter );
   return result_l;
}

void QGameWidget::mousePressEvent( QMouseEvent * event_p ) {
   if ( Qt::LeftButton != event_p->button() ) 
      return;
   if ( global::data::champ().currentRound() != roundId_m )
      return;
   const auto & game_cl = global::data::champ().rounds().at( roundId_m ).games().at( gameId_m );
   auto * const dialog_l = global::widgets::chooseWinner();
   dialog_l->setGame( game_m->text() );
   dialog_l->setFavorite( favoriteName_m->text(), favoriteNation_m->text() );
   dialog_l->setOutsider( outsiderName_m->text(), outsiderNation_m->text() );
   dialog_l->setGameResult( game_cl.result() );
   const auto result_cl = dialog_l->execChoosing();
   if ( championship::round::game::UNKNOWN != result_cl && result_cl != game_cl.result() ) {
      global::data::champ().setGameResult( gameId_m, result_cl );
      global::widgets::mainWindow()->updateGameResultOnScene( gameId_m );
   }
}

