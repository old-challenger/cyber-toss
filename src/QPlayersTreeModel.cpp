#include <QPlayersTreeModel.h>
#include <globals.h>

void QPlayersTreeModel::add ( const QModelIndex & groupIndex_cp, const QString & nickname_cp, const QString & clanname_cp ) {
   assert( !groupIndex_cp.parent().isValid() && NO_PARENT == ( int ) groupIndex_cp.internalPointer() );
   assert( groupIndex_cp.row() < groupsIndex_m.size() );
   const std::size_t newPlayerId_cl = global::data::champ().addPlayer( groupsIndex_m.at( groupIndex_cp.row() ), 
      nickname_cp, clanname_cp );
   //
   const QModelIndex groupIndex_cl = createIndex( groupIndex_cp.row(), 0, ( void * ) NO_PARENT );
   clearGroup ( groupIndex_cl );
   auto group_cl = global::data::champ().players().find( groupsIndex_m.at( groupIndex_cp.row() ) );
   signalisedFillPlayersFor1Group( group_cl.value(), groupIndex_cp );
   emit dataChanged( groupIndex_cl, createIndex( groupIndex_cp.row(), columnCount() - 1, 
      ( void * ) NO_PARENT ) );
}

void QPlayersTreeModel::clearGroup ( const QModelIndex & groupIndex_cp ) {
   if ( playersIndex_m.at( groupIndex_cp.row() ).size() ) {
      beginRemoveRows( groupIndex_cp, 0, playersIndex_m.at( groupIndex_cp.row() ).size() - 1 );
      playersIndex_m[ groupIndex_cp.row() ].clear();
      endRemoveColumns();
   }
}

std::vector< QString > QPlayersTreeModel::createHeadersVector () {
   std::vector< QString > result_l;
   result_l.reserve( 2 );
   result_l.push_back( global::consts::txt::tr( "���������" ) );
   result_l.push_back( global::consts::txt::tr( "����" ) );
   return result_l;
}

QVariant QPlayersTreeModel::data ( const QModelIndex & index_cp, int role_p = Qt::DisplayRole ) const {
   if ( !index_cp.isValid() )
      return QVariant();
   if ( index_cp.column() > 1 )
      return QVariant();
   switch ( role_p ) {
      case Qt::DisplayRole: {
         const auto parentRow_cl = ( int ) index_cp.internalPointer();
         if ( NO_PARENT == parentRow_cl ) { 
            static const QString empty_scl = global::consts::txt::tr( "�����" ),
                                 number_scl = QLatin1String( "( %1 )" );
            assert( index_cp.row() < groupsIndex_m.size() );
            auto groupName_cl = global::data::champ().groupsNames().find( groupsIndex_m.at( index_cp.row() ) );
            assert( global::data::champ().groupsNames().end() != groupName_cl );
            const int groupSize_cl = playersIndex_m.at( index_cp.row() ).size();
            return index_cp.column() ? number_scl.arg( groupSize_cl ? QString::number( groupSize_cl ) : empty_scl ) 
               : groupName_cl.value();
         } else {
            auto players_cl = global::data::champ().players().find( groupsIndex_m.at( parentRow_cl ) );
            assert( global::data::champ().players().end() != players_cl );
            auto player_cl = players_cl.value().find( playersIndex_m.at( parentRow_cl ).at( index_cp.row() ) );
            assert( players_cl.value().end() != player_cl );
            return index_cp.column() ? player_cl.value().clan() : player_cl.value().nickname(); 
         }
      }
      default: return QVariant();
   }  // switch ( role_p ) {
}

QModelIndex QPlayersTreeModel::index ( int row_p, int column_p, const QModelIndex & parent_p ) const {
   auto parentRow_l = NO_PARENT;
   if ( parent_p.isValid() ) {
      parentRow_l = parent_p.row();
      if ( groupsIndex_m.size() <= parentRow_l || row_p >= playersIndex_m.at( parentRow_l ).size() )
         return QModelIndex();
   } else if( row_p >= groupsIndex_m.size() ) 
         return QModelIndex();
   return createIndex( row_p, column_p, ( void * ) parentRow_l );
}

void QPlayersTreeModel::fillPlayersFor1Group ( const championship::playersGroup_map & group_cp, 
                                              id_index & groupIndex_rp ) {
   groupIndex_rp.reserve( group_cp.size() );
   for ( auto player_it = group_cp.cbegin(); group_cp.cend() != player_it; ++player_it )
      groupIndex_rp.push_back( player_it.key() );
}

QVariant QPlayersTreeModel::headerData ( int section_p, Qt::Orientation /*orientation_p*/, 
                                          int role_p = Qt::DisplayRole ) const {
   switch ( role_p ) {
      case Qt::DisplayRole: return section_p > 1 ? QVariant() : headers().at( section_p );
      default: return QVariant();
   }
}

void QPlayersTreeModel::moveDown ( const QModelIndexList & indexes_cp ) {
   operateSelectedIndexes( indexes_cp, movePlayersFor1GroupDown );
}

void QPlayersTreeModel::moveUp ( const QModelIndexList & indexes_cp ) { 
   operateSelectedIndexes( indexes_cp, movePlayersFor1GroupUp );
}

QModelIndex QPlayersTreeModel::parent( const QModelIndex & index_p ) const {
   if ( !index_p.isValid() )
      return QModelIndex();
   const auto parentRow_cl = ( int ) index_p.internalPointer();
   assert( NO_PARENT == parentRow_cl || parentRow_cl < groupsIndex_m.size() );
   /* !!! ����� ������� ������ ���� ����� ���� ��� ���� ��������, ����� ����������� ����� �������� �������� 
   ��������� ���������� Qt */
   return NO_PARENT == parentRow_cl 
      ? QModelIndex() 
      : createIndex( static_cast< int >( parentRow_cl ), 0, ( void * ) NO_PARENT );
}

void QPlayersTreeModel::rebuildIndex() {
   beginResetModel();
   groupsIndex_m.clear();
   playersIndex_m.clear();
   assert( groupsIndex_m.empty() && playersIndex_m.empty() );
   auto groups_cl = global::data::champ().players();
   groupsIndex_m.reserve( groups_cl.size() );
   playersIndex_m.reserve( groups_cl.size() );
   int groupIndex_l = 0;
   for ( auto group_it = groups_cl.cbegin(); groups_cl.cend() != group_it; ++group_it ) {
      groupsIndex_m.push_back( group_it.key() );
      playersIndex_m.push_back( id_index() );
      fillPlayersFor1Group( group_it.value(), playersIndex_m.back() );
      groupIndex_l = playersIndex_m.size();
   }
   endResetModel();
}

void QPlayersTreeModel::remove( const QModelIndexList & indexes_cp ) {
   operateSelectedIndexes( indexes_cp, removePlayersFor1Group );
}

void QPlayersTreeModel::movePlayersFor1GroupDown ( int groupIndex_p, const id_index & groupsIds_cp,
      const championship::ids_list & idsForMove_cp, id_set & groupsForUpdate_p ) {
   if ( groupsIds_cp.size() - 1 == groupIndex_p )   
      return ;
   global::data::champ().movePlayersDown( groupsIds_cp.at( groupIndex_p ), idsForMove_cp );
   groupsForUpdate_p.insert( groupIndex_p );
   groupsForUpdate_p.insert( groupIndex_p + 1 );
}

void QPlayersTreeModel::movePlayersFor1GroupUp ( int groupIndex_p, const id_index & groupsIds_cp,
      const championship::ids_list & idsForMove_cp, id_set & groupsForUpdate_p ) {
   if ( 0 == groupIndex_p )   
      return ;
   global::data::champ().movePlayersUp( groupsIds_cp.at( groupIndex_p ), idsForMove_cp );
   groupsForUpdate_p.insert( groupIndex_p - 1 );
   groupsForUpdate_p.insert( groupIndex_p );
}

void QPlayersTreeModel::operateSelectedIndexes ( const QModelIndexList & indexes_cp, 
      void ( * operation_p ) ( int, const id_index &, const championship::ids_list &, id_set & ) ) {
   int   currentGroupIndex_l  = NO_PARENT,
         nextGroupIndex_l     = NO_PARENT;
   championship::ids_list idsForOperate_l;
   id_set groupsForUpdate_l; 
   for ( auto index_it : indexes_cp ) {
      nextGroupIndex_l = ( int ) index_it.internalPointer();
      if ( nextGroupIndex_l != currentGroupIndex_l ) {
         if ( !idsForOperate_l.empty() ) {
            operation_p( currentGroupIndex_l, groupsIndex_m, idsForOperate_l, groupsForUpdate_l );
            idsForOperate_l.clear();
         }
         currentGroupIndex_l = nextGroupIndex_l;
      }
      auto playerIndex_cl = index_it.row();
      idsForOperate_l.push_back( playersIndex_m.at( currentGroupIndex_l ).at( playerIndex_cl ) );
   }
   if ( !idsForOperate_l.empty() )
      operation_p( currentGroupIndex_l, groupsIndex_m, idsForOperate_l, groupsForUpdate_l );
   updateIndexForGroupsSet( groupsForUpdate_l );
}

void QPlayersTreeModel::removePlayersFor1Group ( int groupIndex_p, const id_index & groupsIds_cp,
      const championship::ids_list & idsForDelete_cp, id_set & groupsForUpdate_p ) {
   global::data::champ().deletePlayers( groupsIds_cp.at( groupIndex_p ), idsForDelete_cp );
   groupsForUpdate_p.insert( groupIndex_p );
}

void QPlayersTreeModel::signalisedFillPlayersFor1Group ( const championship::playersGroup_map & group_cp, 
                                     const QModelIndex & groupIndex_cp ){
   if ( !group_cp.size() )
      return ;
   beginInsertRows( groupIndex_cp, 0, group_cp.size() - 1 );
   fillPlayersFor1Group( group_cp, playersIndex_m[ groupIndex_cp.row() ] );
   endInsertRows();
   emit dataChanged( groupIndex_cp, 
      createIndex( groupIndex_cp.row(), columnCount() - 1, ( void * ) NO_PARENT ) );
}


int QPlayersTreeModel::rowCount ( const QModelIndex & parent_p ) const {
   // ��� ������ �������� ������
   if( !parent_p.isValid() )
      return groupsIndex_m.size();
   // ��� �������� ���� ������� ������
   if ( parent_p.parent().isValid() )
      return 0;
   // ��� �������� ������� ������
   assert( parent_p.row() < playersIndex_m.size() );
   return playersIndex_m.at( parent_p.row() ).size();
}

void QPlayersTreeModel::updateIndexForGroupsSet ( const id_set & groupsForUpdate_cp ) {
   QModelIndex currentGroupIndex_l;
   for ( const auto groupIndex : groupsForUpdate_cp ) {
      currentGroupIndex_l = createIndex( groupIndex, 0, ( void * ) NO_PARENT );
      clearGroup( currentGroupIndex_l );
      signalisedFillPlayersFor1Group ( 
         global::data::champ().players().find( groupsIndex_m.at( groupIndex ) ).value(),
         currentGroupIndex_l );
   }
}



