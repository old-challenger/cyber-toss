#include <championship.h>
#include <globals.h>

#include <uFunctions.h>

#include <QDomDocument>
#include <QTextStream>
#include <QFile>

#include <cstdlib>
#include <ctime>

class consts {
public:
   static const QString & amount () {
      static const QString txt_scl = QLatin1String( "amount" );
      return txt_scl;
   }

   static const QString & clanname () {
      static const QString txt_scl = QLatin1String( "clanname" );
      return txt_scl;
   }

   static const QString & current () {
      static const QString txt_scl = QLatin1String( "current" );
      return txt_scl;
   }

   static const QString & favorite () {
      static const QString txt_scl = QLatin1String( "favorite" );
      return txt_scl;
   }

   static const QString & game () {
      static const QString txt_scl = QLatin1String( "game" );
      return txt_scl;
   }

   static const QString & games () {
      static const QString txt_scl = QLatin1String( "games" );
      return txt_scl;
   }

   static const QString & gid () {
      static const QString txt_scl = QLatin1String( "gid" );
      return txt_scl;
   }

   static const QString & group () {
      static const QString txt_scl = QLatin1String( "group" );
      return txt_scl;
   }

   static const QString & groups () {
      static const QString txt_scl = QLatin1String( "groups" );
      return txt_scl;
   }

   static const QString & list () {
      static const QString txt_scl = QLatin1String( "list" );
      return txt_scl;
   }

   static const QString & name () {
      static const QString txt_scl = QLatin1String( "name" );
      return txt_scl;
   }

   static const QString & nation () {
      static const QString txt_scl = QLatin1String( "nation" );
      return txt_scl;
   }

   static const QString & nations () {
      static const QString txt_scl = QLatin1String( "nations" );
      return txt_scl;
   }

   static const QString & nickname () {
      static const QString txt_scl = QLatin1String( "nickname" );
      return txt_scl;
   }

   static const QString & nid () {
      static const QString txt_scl = QLatin1String( "nid" );
      return txt_scl;
   }

   static const QString & outsider () {
      static const QString txt_scl = QLatin1String( "outsider" );
      return txt_scl;
   }

   static const QString & pid () {
      static const QString txt_scl = QLatin1String( "pid" );
      return txt_scl;
   }

   static const QString & player () {
      static const QString txt_scl = QLatin1String( "player" );
      return txt_scl;
   }

   static const QString & players () {
      static const QString txt_scl = QLatin1String( "players" );
      return txt_scl;
   }

   static const QString & result () {
      static const QString txt_scl = QLatin1String( "result" );
      return txt_scl;
   }

   static const QString & root () {
      static const QString txt_scl = QLatin1String( "root" );
      return txt_scl;
   }

   static const QString & round () {
      static const QString txt_scl = QLatin1String( "round" );
      return txt_scl;
   }

   static const QString & rounds () {
      static const QString txt_scl = QLatin1String( "rounds" );
      return txt_scl;
   }
private:
   consts () { ; }      
};

template< class container_type >
void fillContainer ( const container_type & sourceContainer_cp, championship::ids_vector & targetContaier_rp );

template< >
void fillContainer< championship::ids_vector >( const championship::ids_vector & sourceContainer_cp, 
      championship::ids_vector & targetContaier_rp ) {
   targetContaier_rp = sourceContainer_cp;
}

template< >
void fillContainer< championship::playersGroup_map >( const championship::playersGroup_map & sourceContainer_cp, 
      championship::ids_vector & targetContaier_rp ) {
   for( auto player_it = sourceContainer_cp.cbegin(); sourceContainer_cp.cend() != player_it; ++player_it )
      targetContaier_rp.push_back( player_it.key() );
}

bool championship::loadDataFromFile ( const QString & fileName_cp ) {
   QFile loadFile_l( fileName_cp );
   if( !loadFile_l.open( QFile::ReadOnly ) )
      return false;
   //
   QTextStream intStream_l( &loadFile_l );
   QDomDocument xmlDocument_l;
   const QString test_cl = global::consts::txt::fileType();
   if ( !xmlDocument_l.setContent( intStream_l.readAll() ) 
         || xmlDocument_l.doctype().name() != global::consts::txt::fileType() )
      return false;
   // ���� ������� ������ ������ ���� ����
   QDomNode node_l = xmlDocument_l.firstChild(), level2_l, level3_l, level4_l, level5_l, level6_l;
   // ���� ������ ���� ���������, ���������� "root" � ��������� ��� �������
   if ( !node_l.isElement() && consts::root() != node_l.toElement().tagName()  )
      return false;
   // ���������� ��� ���������� �������� ������ ����������� �� �����
   groups_map     players_l;
   ids_vector     nations_l;
   strings_map    groupsNames_l,
                  nationsNames_l;
   rounds_vector  rounds_l;
   size_type      currentRound_l = 0,
                  roundId_l;
   // ������ ������� ��������
   node_l = node_l.firstChild();
   QDomElement level1Element_l;
   while ( !node_l.isNull() ) {
      if ( !node_l.isElement() )
         return false;
      level1Element_l = node_l.toElement();
      QString tagName_l = level1Element_l.tagName();
      if ( consts::groups() == tagName_l ) {
         level2_l = node_l.firstChild();
         while ( !level2_l.isNull() ) {
            // ���������� �����
            if ( level2_l.isElement() ) {
               QDomElement groupElement_l = level2_l.toElement();
               if ( consts::group() == groupElement_l.tagName() ) {
                  const QString groupIdTxt_cl = groupElement_l.attribute( consts::gid() );
                  if ( !groupIdTxt_cl.isEmpty() ) {
                     const size_type groupId_cl = groupIdTxt_cl.toInt();
                     playersGroup_map nextGroup_l;
                     QString groupName_l;
                     level3_l = level2_l.firstChild();
                     // ���������� ��������� ������
                     while ( !level3_l.isNull() && level3_l.isElement() ) {
                        QDomElement inGroupElement_l = level3_l.toElement();
                        // �������� ������
                        if ( consts::name() == inGroupElement_l.tagName() ) {
                           QDomNode textNode_l = inGroupElement_l.firstChild();
                           if ( textNode_l.isText() ) 
                              groupName_l = textNode_l.toText().data();
                        // ��������� ����� 
                        } else if ( consts::player() == inGroupElement_l.tagName() ) {
                           const QString playerIdTxt_cl = inGroupElement_l.attribute( consts::pid() );
                           if ( !playerIdTxt_cl.isEmpty() ) {
                              const size_type playerId_cl = playerIdTxt_cl.toInt();
                              QString  nickname_l,
                                       clanname_l;
                              level4_l = inGroupElement_l.firstChild();
                              while ( !level4_l.isNull() && level4_l.isElement() ) {
                                 QDomElement inPlayerElement_l = level4_l.toElement();
                                 QDomNode textNode_l = level4_l.firstChild();
                                 if ( textNode_l.isText() ) {
                                    if ( consts::nickname() == inPlayerElement_l.tagName() )
                                       nickname_l = textNode_l.toText().data();
                                    else if ( consts::clanname() == inPlayerElement_l.tagName() )
                                       clanname_l = textNode_l.toText().data();
                                 }
                                 level4_l = level4_l.nextSibling();
                              } // while ( !level4_l.isNull() && level4_l.isElement() ) {
                              if ( !nickname_l.isEmpty() )
                                 nextGroup_l.insert( playerId_cl, player( nickname_l,clanname_l ) );
                           } // if ( !playerIdTxt_cl.isEmpty() ) {
                        } // if ( consts::name() == inGroupElement_l.tagName() ) {
                        level3_l = level3_l.nextSibling();
                     } // while ( !level3_l.isNull() ) {
                     // ���������� ������ � ����������
                     if ( !groupName_l.isEmpty() ) {
                        players_l.insert( groupId_cl, nextGroup_l );
                        groupsNames_l.insert( groupId_cl, groupName_l );
                     }
                  } // if ( !groupIdTxt_cl.isEmpty() ) { 
               } // if ( consts::group() == groupElement_l.tagName() ) {
            } // if ( level2_l.isElement() ) {
            level2_l = level2_l.nextSibling();
         } // while ( !level2_l.isNull() ) {
      } else if  ( consts::nations() == tagName_l ) {
         level2_l = node_l.firstChild();
         while ( !level2_l.isNull() ) {
            if ( level2_l.isElement() ) {
               QDomElement nationsElement_l = level2_l.toElement();
               if ( consts::nation() == nationsElement_l.tagName() ) {
                  const QString nationIdTxt_cl = nationsElement_l.attribute( consts::nid() );
                  if ( !nationIdTxt_cl.isEmpty() ) {
                     const size_type nationId_cl = nationIdTxt_cl.toInt();
                     level3_l = nationsElement_l.firstChild();
                     if ( level3_l.isText() ) {
                        const QString nationName_cl = level3_l.toText().data();
                        if ( !nationName_cl.isEmpty() )
                           nationsNames_l.insert( nationId_cl, nationName_cl );
                     }
                  }
               } else if ( consts::list() == nationsElement_l.tagName() ) {
                  level3_l = nationsElement_l.firstChild();
                  if ( level3_l.isText() ) {
                     const QString nationsIdsListTxt_cl = level3_l.toText().data();
                     if( !nationsIdsListTxt_cl.isEmpty() ) {
                        const QStringList nationsIdsList_cl = nationsIdsListTxt_cl.split( 
                           *global::consts::txt::space().begin() );
                        nations_l.reserve( nationsIdsList_cl.size() );
                        for ( auto nationIdTxt : nationsIdsList_cl )
                           if ( !nationIdTxt.isEmpty() )
                              nations_l.push_back( nationIdTxt.toInt() );
                     } // if( !nationsIdsListTxt_cl.isEmpty() ) {
                  }  // if ( level3_l.isText() ) {
               } // if ( consts::nation() == nationsElement_l.tagName() ) {
            } // if ( level2_l.isElement() ) {
            level2_l = level2_l.nextSibling(); 
         } // while ( !level2_l.isNull() ) {
      } else if  ( consts::rounds()== tagName_l ) {
         const QString  roundsAmountTxt_cl = level1Element_l.attribute( consts::amount() ),
                        currentRoundTxt_cl = level1Element_l.attribute( consts::current() );
         if ( !roundsAmountTxt_cl.isEmpty() && !currentRoundTxt_cl.isEmpty() ) {
            currentRound_l = currentRoundTxt_cl.toInt();
            const size_type roundsAmount_cl = roundsAmountTxt_cl.toInt();
            rounds_l.reserve( roundsAmount_cl );
            if ( roundsAmount_cl )
               for ( auto i = 1 << ( roundsAmount_cl - 1 ); i; i >>= 1 )
                  rounds_l.push_back( round( i ) );
            roundId_l = 0;
            level2_l = node_l.firstChild();
            while ( !level2_l.isNull() && roundId_l < rounds_l.size() ) {
               if ( level2_l.isElement() && consts::round() == level2_l.toElement().tagName() ) {
                  level3_l = level2_l.firstChild();
                  while ( !level3_l.isNull() ) {
                     if ( level3_l.isElement() ) {
                        tagName_l = level3_l.toElement().tagName();
                        if ( consts::games() == tagName_l ) {
                           round::games_vector games_l;
                           games_l.reserve( rounds_l.at( roundId_l ).denominator() );
                           QDomElement gameElement_l;
                           level4_l = level3_l.firstChild();
                           while ( !level4_l.isNull() ) {
                              if ( level4_l.isElement() 
                                 && consts::game() == ( gameElement_l = level4_l.toElement() ).tagName() ) {
                                    level5_l = level4_l.firstChild();
                                    int paramsCounter_l = 0;
                                    round::game::result_enum gameResult_l;
                                    size_type  favoriteGroupId_l, outsiderGroupId_l, favoritePlayerId_l, 
                                       outsiderPlayerId_l,favoriteNationId_l, outsiderNationId_l;
                                    while ( !level5_l.isNull() ) {
                                       if ( level5_l.isElement() ) {
                                          QDomElement gameParamElement_l = level5_l.toElement();
                                          if ( consts::result() == gameParamElement_l.tagName() ) {
                                             level6_l = gameParamElement_l.firstChild();
                                             if ( level6_l.isText() ) {
                                                gameResult_l = round::game::resultFromTxt( level6_l.toText().data() );
                                                ++paramsCounter_l;
                                             }
                                          } else {
                                             const QString  groupIdTxt_cl  = gameParamElement_l.attribute( consts::gid() ),
                                                            playerIdTxt_cl = gameParamElement_l.attribute( consts::pid() ),
                                                            nationIdTxt_cl = gameParamElement_l.attribute( consts::nid() );
                                             if ( !groupIdTxt_cl.isEmpty() && !playerIdTxt_cl.isEmpty() && !nationIdTxt_cl.isEmpty() ) {
                                                if ( consts::favorite() == gameParamElement_l.tagName() ) {
                                                   favoriteGroupId_l    = groupIdTxt_cl.toInt();
                                                   favoritePlayerId_l   = playerIdTxt_cl.toInt();
                                                   favoriteNationId_l   = nationIdTxt_cl.toInt();
                                                   ++paramsCounter_l;
                                                } else if ( consts::outsider() == gameParamElement_l.tagName() ) {
                                                   outsiderGroupId_l    = groupIdTxt_cl.toInt();
                                                   outsiderPlayerId_l   = playerIdTxt_cl.toInt();
                                                   outsiderNationId_l   = nationIdTxt_cl.toInt();
                                                   ++paramsCounter_l;
                                                }
                                             }  // if ( !groupIdTxt_cl.isEmpty() && !playerIdTxt_cl.isEmpty() && !nationIdTxt_cl.isEmpty() ) {
                                          }  // if ( consts::result() == gameParamElement_l.tagName() ) {
                                       }  // if ( level5_l.isElement() ) {
                                       level5_l = level5_l.nextSibling();
                                    }  // while ( !level5_l.isNull() ) {
                                    // ���������� ��������� ����
                                    if ( 3 == paramsCounter_l ) 
                                       games_l.push_back( round::game( round::player_pair( favoriteGroupId_l, favoritePlayerId_l ), favoriteNationId_l, 
                                          round::player_pair( outsiderGroupId_l, outsiderPlayerId_l ), outsiderNationId_l, gameResult_l ) );
                              }  // if ( level4_l.isElement() &&  ..
                              level4_l = level4_l.nextSibling();
                           }  // while ( !level4_l.isNull() ) {
                           rounds_l[ roundId_l ].addGames( games_l );
                        } else if ( consts::players() == tagName_l ) {
                           QDomElement playerElement_l;
                           level4_l = level3_l.firstChild();
                           while ( !level4_l.isNull() ) {
                              if ( level4_l.isElement() ) {
                                 QDomElement groupElement_l = level4_l.toElement();
                                 if ( consts::group() == groupElement_l.tagName() ) {
                                    const QString groupIdTxt_cl = groupElement_l.attribute( consts::gid() );
                                    if ( !groupIdTxt_cl.isEmpty() ) {
                                       const size_type groupId_cl = groupIdTxt_cl.toInt();
                                       level5_l = groupElement_l.firstChild();
                                       if ( level5_l.isText() ) {
                                          const QString playersIdsString_cl = level5_l.toText().data();
                                          if( !playersIdsString_cl.isEmpty() ) {
                                             const QStringList playersIdsList_cl = playersIdsString_cl.split( 
                                                *global::consts::txt::space().begin() );
                                             ids_vector playersIds_l;
                                             playersIds_l.reserve( playersIdsList_cl.size() );
                                             for ( auto playerIdTxt : playersIdsList_cl ) 
                                                if ( !playerIdTxt.isEmpty() )
                                                   playersIds_l.push_back( playerIdTxt.toInt() );
                                             rounds_l[ roundId_l ].addPlayersGroup( groupId_cl, playersIds_l );
                                          } // if( !playersIdsListTxt_cl.isEmpty() ) {
                                       }  // if ( level5_l.isText() ) {
                                    } // if ( !groupIdTxtx_cl.isEmpty() ) {
                                 }  // if ( consts::group() == groupElement_l.tagName() ) {
                              }  // if ( level4_l.isElement() ) {
                              level4_l = level4_l.nextSibling();
                           }  // while ( !level4_l.isNull() ) {
                        } // if ( consts::games() == tagName_l ) {
                     } // if ( level2_l.isElement() ) {
                     level3_l = level3_l.nextSibling();
                  }  // while ( !level3_l.isNull() ) {
               } // if ( level2_l.isElement() && consts::round() == level2_l.toElement().tagName() ) {
               level2_l = level2_l.nextSibling();
               ++roundId_l;
            } // while ( !level2_l.isNull() ) {
         }  // if ( !roundsAmountTxt_cl.isEmpty() && !currentRoundTxt_cl.isEmpty() ) {
      } // if ( consts::groups() == tagName_l ) {
      node_l = node_l.nextSibling();
   } // while ( !node_l.isNull() ) {
   //
   // ���� ������ ��������� �� ������������ � ���������
   if ( !players_l.size() || !nationsNames_l.size() ) 
      return false;
   assert( players_l.size() == groupsNames_l.size() );
   players_m = players_l;
   groupsNames_m = groupsNames_l;
   nationsNames_m = nationsNames_l;
   nations_m = nations_l;
   rounds_m = rounds_l;
   currentRound_m = currentRound_l;
   //
   loadFile_l.close();
   fileName_m = fileName_cp;
   return true;
}

QString championship::makeIdsList ( const ids_vector & ids_cp ) {
   QString result_l;
   for ( auto it : ids_cp )
      result_l += QString::number( it ) + global::consts::txt::space();
   return result_l;
}

void championship::movePlayersDown ( size_type goupId_p, const ids_list & idsForDelete_cp ) {
   auto found_l = players_m.find( goupId_p );
   assert( players_m.end() != found_l );
   if( found_l.key() == ( players_m.constEnd() - 1 ).key() )
      return ;
   auto downGroup_l = found_l + 1;
   insertPlayersInNearGroup( found_l.value(), idsForDelete_cp, downGroup_l.value() );
   deletePlayers( idsForDelete_cp, found_l.value() );
}

void championship::movePlayersUp ( size_type goupId_p, const ids_list & idsForDelete_cp ) {
   auto found_l = players_m.find( goupId_p );
   assert( players_m.end() != found_l );
   if( found_l.key() == players_m.constBegin().key() )
      return ;
   auto upGroup_l = found_l - 1;
   insertPlayersInNearGroup( found_l.value(), idsForDelete_cp, upGroup_l.value() );
   deletePlayers( idsForDelete_cp, found_l.value() );
}

void championship::reserveRounds () {
   assert( !( rounds_m.size() || currentRound_m ) );
   const championship::size_type playersAmount_cl = playersAmount();
   const auto  intBinLog_cl               = utl::intBinLog( playersAmount_cl, 1 ),
               roundsPower_cl             = playersAmount_cl > ( 1 << intBinLog_cl ) ? intBinLog_cl + 1 : intBinLog_cl,
               maxPlayersInFirstTour_cl   = 1 << roundsPower_cl,
               favoritesAmount_cl         = maxPlayersInFirstTour_cl - playersAmount_cl;
   rounds_m.reserve( roundsPower_cl + 1 );
   for ( size_type i = maxPlayersInFirstTour_cl / 2; 0 != i; i >>= 1 )
      rounds_m.push_back( round( i ) );
   //
   auto currentGroup_l = players_m.cbegin();
   championship::size_type favoritesAmountRest_l = favoritesAmount_cl;
   int roundId_l = utl::sign( favoritesAmountRest_l );
   for ( ; players_m.cend() != currentGroup_l; ++currentGroup_l ) {
      if ( favoritesAmountRest_l && favoritesAmountRest_l < currentGroup_l.value().size() ) {
         ids_vector  favoritesGroup_l,
                     outsidersGroup_l;
         randomPlayersGroupSeparation( currentGroup_l.value(), favoritesAmountRest_l, favoritesGroup_l, 
            outsidersGroup_l );
         rounds_m[ roundId_l ].addPlayersGroup( currentGroup_l.key(), favoritesGroup_l );
         roundId_l = 0;
         rounds_m[ roundId_l ].addPlayersGroup( currentGroup_l.key(), outsidersGroup_l );
         favoritesAmountRest_l = 0;
      } else {
         rounds_m[ roundId_l ].addPlayersGroup( currentGroup_l.key(), currentGroup_l.value() );                  
         if ( favoritesAmountRest_l ) 
            if ( !( favoritesAmountRest_l -= currentGroup_l.value().size() ) )
               roundId_l = 0;
         assert( favoritesAmountRest_l >= 0 );
         assert( players_m.cend() != currentGroup_l );
      }  // if ( i < currentGroup_l.value().size() ) {
   } // for ( championship::size_type i = favoritesNumber_cl; 0 != i;  ) {
}

bool championship::roundIsFinished () const {
   const auto nextRoundId_cl = currentRound_m + 1;
   if ( nextRoundId_cl < rounds_m.size() ) {
      const auto & nextRound_crl = rounds_m.at( nextRoundId_cl );
      return nextRound_crl.playersAmount() == nextRound_crl.denominator() * 2;
   } else
      return false;
}

bool championship::saveDataToFile ( const QString & fileName_cp ) {
   const QString fileName_cl = fileName_cp.isEmpty() ? fileName_m : fileName_cp;
   if ( fileName_cl.isEmpty() )
      return false;
   QFile saveFile_l( fileName_cl );
   if( !saveFile_l.open( QFile::WriteOnly ) )
      return false;
   QDomDocument xmlDocument_l( global::consts::txt::fileType() );
   QDomElement root_l = xmlDocument_l.createElement( consts::root() ); {
      // ������
      QDomElement groups_l = xmlDocument_l.createElement( consts::groups() ); {
         for ( auto group_it = players_m.constBegin(); players_m.end() != group_it; ++group_it ) {
            QDomElement group_l = xmlDocument_l.createElement( consts::group() ); 
               group_l.setAttribute( consts::gid(), QString::number( group_it.key() ) );
               QDomElement groupName_l = xmlDocument_l.createElement( consts::name() );
                  assert( groupsNames_m.cend() != groupsNames_m.find( group_it.key() ) );
                  groupName_l.appendChild( xmlDocument_l.createTextNode( 
                     groupsNames_m.value( group_it.key() ) ) );
               group_l.appendChild( groupName_l );
               for ( auto player_it = group_it.value().constBegin(); 
                     group_it.value().constEnd() != player_it; ++player_it ) {
                  QDomElement player_l = xmlDocument_l.createElement( consts::player() );
                     player_l.setAttribute( consts::pid(), QString::number( player_it.key() ) );
                     QDomElement nickname_l = xmlDocument_l.createElement( consts::nickname() ),
                                 clanname_l = xmlDocument_l.createElement( consts::clanname() );
                        nickname_l.appendChild( xmlDocument_l.createTextNode( player_it.value().nickname() ) );
                        clanname_l.appendChild( xmlDocument_l.createTextNode( player_it.value().clan() ) );
                     player_l.appendChild( nickname_l );
                     player_l.appendChild( clanname_l );
                  group_l.appendChild( player_l );
               }
            groups_l.appendChild( group_l );
         } // for ( auto group_it = players_m.constBegin(); players_m.end() != group_it; ++group_it ) {
      } // QDomElement players_l
      root_l.appendChild( groups_l );
      
      // �����
      QDomElement nations_l = xmlDocument_l.createElement( consts::nations() ); {
         for ( auto nation_it = nationsNames_m.constBegin(); nationsNames_m.constEnd() != nation_it; 
               ++nation_it ) {
            QDomElement nation_l = xmlDocument_l.createElement( consts::nation() );
               nation_l.setAttribute( consts::nid(), nation_it.key() );
               nation_l.appendChild(  xmlDocument_l.createTextNode( nation_it.value() ) );
            nations_l.appendChild( nation_l );
         }
         QDomElement nationsList_l = xmlDocument_l.createElement( consts::list() );
            nationsList_l.appendChild( xmlDocument_l.createTextNode( makeIdsList( global::data::champ().nations() ) ) );
         nations_l.appendChild( nationsList_l );
      } // QDomElement nations_l 
      root_l.appendChild( nations_l );

      // ����� ����������
      QDomElement rounds_l = xmlDocument_l.createElement( consts::rounds() ); {
         rounds_l.setAttribute( consts::amount(),  QString::number( global::data::champ().rounds().size() ) );
         rounds_l.setAttribute( consts::current(), currentRound_m );
         for ( auto round_it : global::data::champ().rounds() ) {
            if ( !round_it.games().size() && !round_it.playersAmount() )
               break;
            QDomElement round_l = xmlDocument_l.createElement( consts::round() ); {
               QDomElement games_l = xmlDocument_l.createElement( consts::games() );
               for ( auto game_it : round_it.games() ) {
                  QDomElement game_l = xmlDocument_l.createElement( consts::game() ); {
                     QDomElement favorite_l  = xmlDocument_l.createElement( consts::favorite() ),
                                 outsider_l  = xmlDocument_l.createElement( consts::outsider() ),
                                 result_l    = xmlDocument_l.createElement( consts::result() );
                        favorite_l.setAttribute( consts::gid(), game_it.favorite().groupId()    );
                        favorite_l.setAttribute( consts::pid(), game_it.favorite().playerId()   );
                        favorite_l.setAttribute( consts::nid(), game_it.favoriteNation()        );
                        outsider_l.setAttribute( consts::gid(), game_it.outsider().groupId()    );
                        outsider_l.setAttribute( consts::pid(), game_it.outsider().playerId()   );
                        outsider_l.setAttribute( consts::nid(), game_it.outsiderNation()        );
                        result_l.appendChild( xmlDocument_l.createTextNode( game_it.resultTxt( game_it.result() ) ) );
                     game_l.appendChild( favorite_l );
                     game_l.appendChild( outsider_l );
                     game_l.appendChild( result_l   );
                  }
                  games_l.appendChild( game_l );
               }
               round_l.appendChild( games_l );
               QDomElement players_l = xmlDocument_l.createElement( consts::players() );
               for ( auto group_it = round_it.players().constBegin(); 
                     round_it.players().constEnd() != group_it; ++group_it ) {
                  QDomElement group_l = xmlDocument_l.createElement( consts::group() );
                     group_l.setAttribute( consts::gid(),      group_it.key() );
                     group_l.appendChild( xmlDocument_l.createTextNode( makeIdsList( group_it.value() ) ) );
                  players_l.appendChild( group_l );
               }
               round_l.appendChild( players_l );
            }
            rounds_l.appendChild( round_l );
         }   
      } // QDomElement games_l 
      root_l.appendChild( rounds_l );
   } // QDomElement root_l 
   xmlDocument_l.appendChild( root_l );
   //
   QTextStream outStream_l( &saveFile_l );
   outStream_l << xmlDocument_l.toString();
   saveFile_l.close();
   if( fileName_m != fileName_cl )
      fileName_m = fileName_cl;
   return true;
}

void championship::randomPlayersGroupSeparation ( const playersGroup_map & sourceGroup_cp, 
      int firstPartSize_p, ids_vector & firstPart_rp, ids_vector & secondPart_rp ) {
   ids_vector allPlayersIds_l;
   allPlayersIds_l.reserve( sourceGroup_cp.size() );
   for ( auto player_it = sourceGroup_cp.cbegin(); sourceGroup_cp.cend() != player_it; ++player_it )
      allPlayersIds_l.push_back( player_it.key() );
   ids_set chousenPlayersSet_l;
   ids_vector::size_type nextId_l;
   srand( time( nullptr ) );
   for ( auto i = firstPartSize_p; i; ) {
      nextId_l = static_cast< ids_vector::size_type >( rand() ) % allPlayersIds_l.size();
      if ( chousenPlayersSet_l.end() == chousenPlayersSet_l.find( allPlayersIds_l.at( nextId_l ) ) ) {
         chousenPlayersSet_l.insert( allPlayersIds_l.at( nextId_l ) );
         --i;
      }
   }
   assert( !( firstPart_rp.size() || secondPart_rp.size() ) );
   secondPart_rp.reserve( firstPartSize_p );
   firstPart_rp.reserve( allPlayersIds_l.size() - firstPartSize_p );
   for ( auto id : allPlayersIds_l )
      if ( chousenPlayersSet_l.end() == chousenPlayersSet_l.find( id ) )
         secondPart_rp.push_back( id );
      else 
         firstPart_rp.push_back( id );
}

void championship::setDefaultValues () {
   players_m.clear();
   players_m.insert( 1, championship::playersGroup_map() );
   players_m.insert( 2, championship::playersGroup_map() );
   players_m.insert( 3, championship::playersGroup_map() );
   players_m.insert( 4, championship::playersGroup_map() );

   static const QString championsTxt_scl  = global::consts::txt::tr( "��������" ),
                        mastersTxt_scl    = global::consts::txt::tr( "�������" ),
                        veteransTxt_scl   = global::consts::txt::tr( "��������" ),
                        beginnersTxt_scl  = global::consts::txt::tr( "�������" );
   //
   groupsNames_m.clear();
   groupsNames_m.insert( 1, championsTxt_scl );
   groupsNames_m.insert( 2, mastersTxt_scl   );
   groupsNames_m.insert( 3, veteransTxt_scl  );
   groupsNames_m.insert( 4, beginnersTxt_scl );

   static const QString aztecsTxt_scl        = global::consts::txt::tr( "������" ),
                        greatBritainTxt_scl  = global::consts::txt::tr( "��������������" ),
                        germanyTxt_scl       = global::consts::txt::tr( "��������" ),
                        huronTxt_scl         = global::consts::txt::tr( "������" ),
                        delawareTxt_scl      = global::consts::txt::tr( "��������" ),
                        incaTxt_scl          = global::consts::txt::tr( "����" ),
                        iroquoisTxt_scl      = global::consts::txt::tr( "�������" ),
                        spainTxt_scl         = global::consts::txt::tr( "�������" ),
                        maya_scl             = global::consts::txt::tr( "����" ),
                        netherlandsTxt_scl   = global::consts::txt::tr( "����������" ),
                        portugalTxt_scl      = global::consts::txt::tr( "����������" ),
                        puebloTxt_scl        = global::consts::txt::tr( "������" ),
                        russiaTxt_scl        = global::consts::txt::tr( "������" ),    
                        siouxTxt_scl         = global::consts::txt::tr( "���" ),    
                        usaTxt_scl           = global::consts::txt::tr( "���" ),       
                        franceTxt_scl        = global::consts::txt::tr( "�������" ),
                        haydaTxt_scl         = global::consts::txt::tr( "�����" );   
   //
   nationsNames_m.clear();
   nationsNames_m.insert(  1, aztecsTxt_scl        );
   nationsNames_m.insert(  2, greatBritainTxt_scl  );
   nationsNames_m.insert(  3, germanyTxt_scl       );
   nationsNames_m.insert(  4, huronTxt_scl         );
   nationsNames_m.insert(  5, delawareTxt_scl      );
   nationsNames_m.insert(  6, incaTxt_scl          );
   nationsNames_m.insert(  7, iroquoisTxt_scl      );
   nationsNames_m.insert(  8, spainTxt_scl         );
   nationsNames_m.insert(  9, maya_scl             );
   nationsNames_m.insert( 10, netherlandsTxt_scl   );
   nationsNames_m.insert( 11, portugalTxt_scl      );
   nationsNames_m.insert( 12, puebloTxt_scl        );
   nationsNames_m.insert( 13, russiaTxt_scl        );
   nationsNames_m.insert( 14, siouxTxt_scl         );
   nationsNames_m.insert( 15, usaTxt_scl           );
   nationsNames_m.insert( 16, franceTxt_scl        );
   nationsNames_m.insert( 17, haydaTxt_scl         );

   nations_m.clear();
   nations_m.reserve( nationsNames_m.size() );
   for ( auto nation_it = nationsNames_m.constBegin(); nationsNames_m.constEnd() != nation_it; ++nation_it )
      nations_m.push_back( nation_it.key() );

   rounds_m.clear();
   currentRound_m = 0;
}

void championship::setGameResult ( size_type gameId_p, round::game::result_enum result_p ) {
   const auto & currentGame_crl = rounds_m.at( currentRound_m ).games().at( gameId_p );
   const auto nextRoundId_cl = currentRound_m + 1;
   if ( nextRoundId_cl < rounds_m.size() ) {
      if ( round::game::UNKNOWN != currentGame_crl.result() )
         rounds_m[ nextRoundId_cl ].removePlayer( currentGame_crl.winner() );
      rounds_m[ currentRound_m ].setGameResult( gameId_p, result_p );   
      rounds_m[ nextRoundId_cl ].addPlayer( currentGame_crl.winner() );
   } else
      rounds_m[ currentRound_m ].setGameResult( gameId_p, result_p );   
}
