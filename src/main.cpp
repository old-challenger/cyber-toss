#include <globals.h>
#include <QTossApplication.h>
//
int main ( int num_p, char ** args_p ) {
   QTossApplication app_l( num_p, args_p );
   QTextCodec::setCodecForLocale( global::consts::txt::codec() );
   global::widgets::mainWindow()->show();
   return app_l.exec();
}