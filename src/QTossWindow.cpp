#include <QStringEnterDialog.h>
#include <QTossWindow.h>
#include <globals.h> 
//
#include <QModelIndexList>
#include <QVBoxLayout>
#include <QSplitter>
#include <QToolBar>
#include <QMenuBar>
#include <QSlider>
#include <QFrame>
//
#include <cassert>
#include <set>

QTossWindow::QTossWindow () : QMainWindow(), toosResultsWidget_m( nullptr ), playersTree_m( nullptr ),
   nationsList_m( nullptr ), playersModel_m( nullptr ), nationsModel_m( nullptr ), diagramScene_m( nullptr ), 
   addPlayerBtn_m( nullptr ), removePlayersBtn_m( nullptr ), movePlayerUpBtn_m( nullptr ), 
   movePlayerDownBtn_m( nullptr ), addNationBtn_m( nullptr ), removeNationsBtn_m( nullptr ), 
   scaleLbl_m( nullptr ), newTossAct_m( nullptr ), openTossAct_m( nullptr ), saveTossAct_m( nullptr ), 
   saveTossAsAct_m( nullptr ), exitAct_m( nullptr ), startAct_m( nullptr ), printAct_m( nullptr ), 
   configAct_m( nullptr ), helpAct_m( nullptr ), currentScalePercents_m( 100 ) {
   QSplitter   * const hSplitter_l = new QSplitter( Qt::Orientation::Horizontal ),
               * const vSplitter_l = new QSplitter( Qt::Orientation::Vertical );
   setCentralWidget( hSplitter_l );
   hSplitter_l->setContentsMargins( 3, 3, 3, 3 );
   // ������ ������� � �����
   hSplitter_l->addWidget( vSplitter_l );
      // ������
      vSplitter_l->addWidget( createPlayersWidget() );
      // �����
      vSplitter_l->addWidget( createNationsWidget() );
      vSplitter_l->setStretchFactor( 0, 3 );
      vSplitter_l->setStretchFactor( 1, 2 );
   // ���������            
   hSplitter_l->addWidget( toosResultsWidget_m = 
         new QGraphicsView( diagramScene_m = new QDiagramScene( this ), this ) );
      toosResultsWidget_m->setFrameStyle( QFrame::Shape::Box );
      toosResultsWidget_m->setMinimumWidth( 500 );
   hSplitter_l->setStretchFactor( 0, 0 );
   hSplitter_l->setStretchFactor( 1, 1 );
   //������� �������� hSplitter_l->setHandleWidth( 33 );
   createMenu();
   createToolBar();
   //
   setWindowIcon( global::consts::icons::toss() );
   setWindowTitle( windowTitleWithoutFile() );
   //
}

void QTossWindow::allDataWereChanged () {
   nationsList_m->clearSelection();
   nationsList_m->setCurrentIndex( QModelIndex() );
   nationsList_m->update();
   playersTree_m->clearSelection();
   playersTree_m->setCurrentIndex( QModelIndex() );
   playersModel_m->rebuildIndex();
   diagramScene_m->clearDiagram();
   if ( global::data::champ().rounds().size() )
      diagramScene_m->updateDiagram();
   checkStartButtonEnabled();
}

void QTossWindow::checkStartButtonEnabled() {
   if ( global::data::champ().rounds().size() ) {
      const auto nextRoundId_cl = global::data::champ().currentRound() + 1;
      if ( nextRoundId_cl < global::data::champ().rounds().size() ) {
         const auto & nextRound_crl = global::data::champ().rounds().at( nextRoundId_cl );
         startAct_m->setEnabled( nextRound_crl.playersAmount() == nextRound_crl.denominator() * 2 );
      } else
         startAct_m->setEnabled( false );
   } else 
      startAct_m->setEnabled( global::data::champ().playersAmount() >= championship::MIN_PLAYERS_FOR_TOSS );
}

QFrame * QTossWindow::createPlayersWidget () {
   QFrame * const playersWidget_l = new QFrame( this );
   playersWidget_l->setFrameStyle( QFrame::Shape::Box );
   QVBoxLayout * const playersLayout_l = new QVBoxLayout( playersWidget_l );
      playersLayout_l->addWidget( 
         new QLabel( global::consts::txt::tr( "������ �������" ), playersWidget_l ) );
      QHBoxLayout * const playersButtonsLayout_l = new QHBoxLayout();
      playersLayout_l->addLayout( playersButtonsLayout_l );
         playersButtonsLayout_l->addWidget( addPlayerBtn_m = new QToolButton( playersWidget_l ) );
            addPlayerBtn_m->setIcon( global::consts::icons::plus() );
            addPlayerBtn_m->setToolTip( global::consts::txt::add() );
            addPlayerBtn_m->setEnabled( false );
         playersButtonsLayout_l->addWidget( removePlayersBtn_m = new QToolButton( playersWidget_l ) );
            removePlayersBtn_m->setIcon( global::consts::icons::cross() );
            removePlayersBtn_m->setToolTip( global::consts::txt::remove() );
            removePlayersBtn_m->setEnabled( false );
         playersButtonsLayout_l->addStretch( 1 );
         playersButtonsLayout_l->addWidget( movePlayerUpBtn_m = new QToolButton( playersWidget_l ) );
            movePlayerUpBtn_m->setIcon( global::consts::icons::up() );
            movePlayerUpBtn_m->setToolTip( global::consts::txt::tr( "����������� �� ����" ) );
            movePlayerUpBtn_m->setEnabled( false );
         playersButtonsLayout_l->addWidget( movePlayerDownBtn_m = new QToolButton( playersWidget_l ) );
            movePlayerDownBtn_m->setIcon( global::consts::icons::down() );
            movePlayerDownBtn_m->setToolTip( global::consts::txt::tr( "����������� ����" ) );
            movePlayerDownBtn_m->setEnabled( false );
      playersLayout_l->addWidget( playersTree_m = new QTreeView( playersWidget_l ) );
         playersTree_m->setModel( playersModel_m = new QPlayersTreeModel( playersWidget_l ) );
         playersTree_m->setSelectionMode( QAbstractItemView::ExtendedSelection );
   //
   connect( addPlayerBtn_m,      SIGNAL( released() ), this, SLOT( slotPlayersActions() ) );
   connect( removePlayersBtn_m,  SIGNAL( released() ), this, SLOT( slotPlayersActions() ) );
   connect( movePlayerUpBtn_m,   SIGNAL( released() ), this, SLOT( slotPlayersActions() ) );
   connect( movePlayerDownBtn_m, SIGNAL( released() ), this, SLOT( slotPlayersActions() ) );
   connect( playersTree_m->selectionModel(), SIGNAL( selectionChanged( const QItemSelection &, const QItemSelection & ) ),
      this, SLOT( slotPlayersSelectionsChanged( const QItemSelection &, const QItemSelection & ) ) );
   //
   return playersWidget_l;
}

QFrame * QTossWindow::createNationsWidget () {
   QFrame * const nationsWidget_l = new QFrame( this );
   nationsWidget_l->setFrameStyle( QFrame::Shape::Box );
      QVBoxLayout * const nationsLayout_l = new QVBoxLayout( nationsWidget_l );
      nationsLayout_l->addWidget( 
         new QLabel( global::consts::txt::tr( "����� ��� ���������� ����" ), nationsWidget_l ) );
      QHBoxLayout * const nationsButtonsLayout_l = new QHBoxLayout();
      nationsLayout_l->addLayout( nationsButtonsLayout_l );
         nationsButtonsLayout_l->addWidget( addNationBtn_m = new QToolButton( nationsWidget_l ) );
            addNationBtn_m->setIcon( global::consts::icons::plus() );
            addNationBtn_m->setToolTip( global::consts::txt::add() );
         nationsButtonsLayout_l->addWidget( removeNationsBtn_m = new QToolButton( nationsWidget_l ) );
            removeNationsBtn_m->setIcon( global::consts::icons::cross() );
            removeNationsBtn_m->setToolTip( global::consts::txt::remove() );
            removeNationsBtn_m->setEnabled( false );
         nationsButtonsLayout_l->addStretch( 1 );
      nationsLayout_l->addWidget( nationsList_m = new QListView( nationsWidget_l ) );
         nationsList_m->setModel( nationsModel_m = new QNationsListModel( nationsWidget_l ) );
         nationsList_m->setSelectionMode( QAbstractItemView::ExtendedSelection );
   //
   connect( addNationBtn_m,      SIGNAL( released() ), this, SLOT( slotNationsActions() ) );
   connect( removeNationsBtn_m,  SIGNAL( released() ), this, SLOT( slotNationsActions() ) );
   connect( nationsList_m->selectionModel(), 
      SIGNAL( selectionChanged( const QItemSelection &, const QItemSelection & ) ), this, 
      SLOT( slotNationsSelectionsChanged() ) );
   //
   return nationsWidget_l;
}

void QTossWindow::createMenu () {
   QMenu * const champ_l   = menuBar()->addMenu( global::consts::txt::tr( "���������" ) ),
         * const toss_l    = menuBar()->addMenu( global::consts::txt::tr( "����������" ) );
   // ���������
   champ_l->addAction( newTossAct_m = new QAction( global::consts::icons::create(), 
      global::consts::txt::tr( "������ �����" ), this ) );
      newTossAct_m->setToolTip( global::consts::txt::tr( "������� ����� ���������" ) );
   champ_l->addAction( openTossAct_m = new QAction( global::consts::icons::open(),
      global::consts::txt::tr( "���������� �����������" ), this ) );
      openTossAct_m->setToolTip( global::consts::txt::tr( "������� ����������� ���������" ) );
   champ_l->addAction( saveTossAct_m = new QAction( global::consts::icons::save(),
      global::consts::txt::tr( "���������" ), this ) );
      saveTossAct_m->setToolTip( global::consts::txt::tr( "C�������� ���������" ) );
   champ_l->addAction( saveTossAsAct_m = new QAction( global::consts::icons::saveAs(),
      global::consts::txt::tr( "��������� ��� ..." ), this ) );
      saveTossAsAct_m->setToolTip( global::consts::txt::tr( "C�������� ��������� � ������ ����" ) );
   champ_l->addSeparator();
   champ_l->addAction( exitAct_m = new QAction( global::consts::icons::close(),
      global::consts::txt::tr( "�����" ), this ) );
   // ����������
   toss_l->addAction( startAct_m = new QAction( global::consts::icons::start(), 
      global::consts::txt::tr( "���������" ), this ) );
      startAct_m->setToolTip( global::consts::txt::tr( "�������� ����������" ) );
      startAct_m->setEnabled( false );
   toss_l->addAction( printAct_m = new QAction( global::consts::icons::print(), 
      global::consts::txt::tr( "�������������" ), this ) );
      printAct_m->setToolTip( global::consts::txt::tr( "������������� ����� � Visio" ) );
      printAct_m->setEnabled( false );
   toss_l->addAction( configAct_m = new QAction( global::consts::icons::config(),
      global::consts::txt::tr( "���������" ), this ) );
      configAct_m->setEnabled( false );
   // ������
   helpAct_m = menuBar()->addAction( global::consts::txt::tr( "?" ) );   
   //
   connect( newTossAct_m,     SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( openTossAct_m,    SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( saveTossAct_m,    SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( saveTossAsAct_m,  SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( exitAct_m,        SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( startAct_m,       SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( printAct_m,       SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( configAct_m,      SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
   connect( helpAct_m,        SIGNAL( triggered() ), this, SLOT( slotMenuActions() ) );
}

void QTossWindow::createToolBar () {
   QToolBar * const bar_l = addToolBar( "main" );
   bar_l->addAction( newTossAct_m );
   bar_l->addAction( openTossAct_m );
   bar_l->addAction( saveTossAct_m );
   bar_l->addSeparator();
   bar_l->addAction( startAct_m );
   bar_l->addAction( printAct_m );
   bar_l->addSeparator();
   bar_l->addWidget( new QWidget( bar_l ) );
   bar_l->addSeparator();
   QSlider * const slider_l = new QSlider( Qt::Horizontal, bar_l );
      slider_l->setMinimum( 10 );
      slider_l->setMaximum( 100 );
      slider_l->setTickInterval( 5 );
      slider_l->setValue( 100 );
      slider_l->setMaximumWidth( 113 );
   bar_l->addWidget( new QLabel( global::consts::txt::tr( "�������: " ), bar_l ) );
   bar_l->addWidget( slider_l );
   bar_l->addWidget( scaleLbl_m = new QLabel( global::consts::txt::percents( 100 ), bar_l ) );
   connect( slider_l, SIGNAL( valueChanged( int ) ), this, SLOT( slotScaleDiagram( int ) ) );
}

QString QTossWindow::execSaveAsDialog () {
   static const QString title_scl = global::consts::txt::tr( "����� ����� ��� ����������" );
   global::widgets::fileDialog()->setWindowTitle( title_scl );
   global::widgets::fileDialog()->setAcceptMode( QFileDialog::AcceptSave );
   global::widgets::fileDialog()->selectFile( global::data::champ().fileName() );
   if ( QDialog::Accepted == global::widgets::fileDialog()->exec() ) {
      assert( global::widgets::fileDialog()->selectedFiles().size() == 1 );
      return global::widgets::fileDialog()->selectedFiles().front();
   }
   return QString();
}

QString QTossWindow::makeMoveElementsText ( int elementsListSize_p ) {
   static const QString singleMoved_scl = global::consts::txt::tr( " ���������" ),
                        setMovedPostfix_scl = global::consts::txt::tr( "�" ),
                        forOneGroup_scl = global::consts::txt::tr( " �� ���� ������ " );
   const QString result_cl = QString::number( elementsListSize_p ) + global::consts::txt::space() 
      + ( 1 == elementsListSize_p ? global::consts::txt::singleSelected() : global::consts::txt::setSelected() )
      + global::consts::txt::elements( elementsListSize_p ) + global::consts::txt::space() 
      + global::consts::txt::willBe() + singleMoved_scl 
      + ( 1 == elementsListSize_p ? QString() : setMovedPostfix_scl ) + forOneGroup_scl;
   return result_cl;
}

void QTossWindow::saveDataToFile ( const QString fileName_cp ) {
   static const QString okMessage_scl = global::consts::txt::tr( "������ ������� ��������� � ����\n\"%1\"" ),
                        errorMessage_scl = global::consts::txt::tr( "�� ������� ��������� ������ � ����\n\"%1\"" );
   const bool result_cl = global::data::champ().saveDataToFile( fileName_cp );
   if ( result_cl ) {
      setWindowTitle( windowTitleWithFile() + global::data::champ().fileName() );
      global::widgets::information()->setIcon( QMessageBox::Information );
      global::widgets::information()->setText( okMessage_scl.arg( global::data::champ().fileName() ) );
   } else {
      global::widgets::information()->setIcon( QMessageBox::Warning );
      global::widgets::information()->setText( errorMessage_scl.arg( global::data::champ().fileName() ) );
   }
   global::widgets::information()->exec();
}

void QTossWindow::slotPlayersActions () {
   const QToolButton * const sender_cl = dynamic_cast< QToolButton * >( sender() );
   assert( NULL != sender_cl );
   const QModelIndexList selectedIndexes_cl = playersTree_m->selectionModel()->selectedRows();
   //
   if       ( sender_cl == addPlayerBtn_m ) {
      static const QString enterStringWindowTitle_scl = 
         global::consts::txt::tr( "������� ��������� ������������ ������" );
      global::widgets::enterPlayerStsrings()->setWindowTitle( enterStringWindowTitle_scl );
      if ( QDialog::Accepted == global::widgets::enterPlayerStsrings()->exec() ) {
         assert( 1 == selectedIndexes_cl.size() && !selectedIndexes_cl.front().parent().isValid() );
         playersModel_m->add( selectedIndexes_cl.front(),
            global::widgets::enterPlayerStsrings()->getString( QPlayersTreeModel::NICKNAME ),
            global::widgets::enterPlayerStsrings()->getString( QPlayersTreeModel::CLAN ) );
         checkStartButtonEnabled();
      }
   }
   else if  ( sender_cl == removePlayersBtn_m ) {
      assert( selectedIndexes_cl.size() );
      static const QString commonBegin_scl = global::consts::txt::tr( "�� ������ ������� " );
      global::widgets::question()->setText( commonBegin_scl + global::consts::txt::willBe()
         + ( 1 == selectedIndexes_cl.size() 
               ? global::consts::txt::singleDeleted() : global::consts::txt::setDeleted() )
         + QString::number( selectedIndexes_cl.size() ) 
         + ( 1 == selectedIndexes_cl.size() ? global::consts::txt::singleSelected() : global::consts::txt::setSelected() )
         + global::consts::txt::elements( selectedIndexes_cl.size() ) + global::consts::txt::dot() );
      //
      global::widgets::question()->exec();
      if ( QMessageBox::Ok == global::widgets::question()->result() ) {
         playersTree_m->clearSelection();
         playersTree_m->setCurrentIndex( QModelIndex() );
         playersModel_m->remove( selectedIndexes_cl );
         checkStartButtonEnabled();
      }
   }
   else if  ( sender_cl == movePlayerUpBtn_m ) {
      assert( selectedIndexes_cl.size() );
      global::widgets::question()->setText( makeMoveElementsText( selectedIndexes_cl.size() )
         + global::consts::txt::tr( "�����" ) );
      global::widgets::question()->exec();
      if ( QMessageBox::Ok == global::widgets::question()->result() ) {
         playersTree_m->clearSelection();
         playersTree_m->setCurrentIndex( QModelIndex() );
         playersModel_m->moveUp( selectedIndexes_cl );
      }
   }
   else if  ( sender_cl == movePlayerDownBtn_m ) {
      assert( selectedIndexes_cl.size() );
      global::widgets::question()->setText( makeMoveElementsText( selectedIndexes_cl.size() )
         + global::consts::txt::tr( "����" ) );
      global::widgets::question()->exec();
      if ( QMessageBox::Ok == global::widgets::question()->result() ) {
         playersTree_m->clearSelection();
         playersTree_m->setCurrentIndex( QModelIndex() );
         playersModel_m->moveDown( selectedIndexes_cl );
      }
   }
   else
      assert( !"[ERROR] Incorrect IF brunch." );
}

void QTossWindow::slotNationsActions () {
   const QToolButton * const sender_cl = dynamic_cast< QToolButton * >( sender() );
   assert( NULL != sender_cl );
   //
   if ( sender_cl == addNationBtn_m ) {
      if ( QDialog::Accepted == global::widgets::chooseNation()->exec() )
         nationsModel_m->add( global::widgets::chooseNation()->currentId() );
   } else if ( sender_cl == removeNationsBtn_m ) {
      // �������� ������ ���������� �����  
      const QModelIndexList selection_cl = nationsList_m->selectionModel()->selectedIndexes();
      assert( selection_cl.size() );
      static const QString commonBegin_scl = global::consts::txt::tr( "�� ������ ����� " );
         global::widgets::question()->setText( commonBegin_scl + global::consts::txt::willBe()
            + ( 1 == selection_cl.size() 
                  ? global::consts::txt::singleDeleted() : global::consts::txt::setDeleted() )
            + QString::number( selection_cl.size() ) 
            + ( 1 == selection_cl.size() ? global::consts::txt::singleSelected() : global::consts::txt::setSelected() )
            + global::consts::txt::elements( selection_cl.size() ) + global::consts::txt::dot() );
      //
      global::widgets::question()->exec();
      if ( QMessageBox::Ok == global::widgets::question()->result() ) {
         nationsList_m->clearSelection();
         nationsList_m->setCurrentIndex( QModelIndex() );
         nationsModel_m->remove( selection_cl );
      }
   } else
      assert( !"[ERROR] Incorrect IF brunch." );
}

void QTossWindow::slotMenuActions () {
   const QAction * const sender_cl = dynamic_cast< QAction * >( sender() );
   assert( NULL != sender_cl );
   if ( sender_cl == newTossAct_m ) {
      static const QString text_scl = 
         global::consts::txt::tr( "����� ������ ����� ���������. ��� ������������� ������ ����� ��������." );
      global::widgets::question()->setText( text_scl );
      global::widgets::question()->exec();
      if ( QMessageBox::Ok == global::widgets::question()->result() ) {
         global::data::champ().setDefaultValues();
         setWindowTitle( windowTitleWithoutFile() );
         allDataWereChanged();
      }
   } else if ( sender_cl == openTossAct_m ) {
      static const QString text_scl = 
         global::consts::txt::tr( "����� ���������� ������ ������������ ����������. ��� ������������� ������ ����� ��������." );
      global::widgets::question()->setText( text_scl );
      global::widgets::question()->exec();
      if ( QMessageBox::Ok == global::widgets::question()->result() ) {
         static const QString title_scl = global::consts::txt::tr( "����� ����� ��� ��������" );
         global::widgets::fileDialog()->setWindowTitle( title_scl );
         global::widgets::fileDialog()->setAcceptMode( QFileDialog::AcceptOpen );
         global::widgets::fileDialog()->selectFile( QString() );
         if ( QDialog::Accepted == global::widgets::fileDialog()->exec() ) {
            assert( 1 == global::widgets::fileDialog()->selectedFiles().size() );
            const QString fileName_cl = global::widgets::fileDialog()->selectedFiles().front();
            const bool result_cl = global::data::champ().loadDataFromFile( fileName_cl );
            if ( result_cl ) {
               static const QString okMessage_scl = 
                  global::consts::txt::tr( "������ ������� ��������� �� �����\n\"%1\"" );
               setWindowTitle( windowTitleWithFile() + global::data::champ().fileName() );
               global::widgets::information()->setIcon( QMessageBox::Information );
               global::widgets::information()->setText( okMessage_scl.arg( global::data::champ().fileName() ) );
               allDataWereChanged();
            } else {
               static const QString errorMessage_scl = 
                  global::consts::txt::tr( "�� ������� ��������� ������ �� �����\n\"%1\"" );
               global::widgets::information()->setIcon( QMessageBox::Warning );
               global::widgets::information()->setText( errorMessage_scl.arg( fileName_cl ) );
            }
            global::widgets::information()->exec();
            global::widgets::mainWindow()->activateWindow();
         }
      }
   } else if ( sender_cl == saveTossAct_m ) {
      if ( global::data::champ().fileName().isEmpty() )
         saveTossAsAct_m->activate( QAction::Trigger );
      else
         saveDataToFile();
   } else if ( sender_cl == saveTossAsAct_m ) {
      const QString fileName_l = execSaveAsDialog();
      if ( !fileName_l.isEmpty() )
         saveDataToFile( fileName_l );;
   } else if ( sender_cl == exitAct_m )
      close();
   else if ( sender_cl == startAct_m ) {
      global::objects::tossThread()->start();
   }
   else if ( sender_cl == printAct_m )
      ;
   else if ( sender_cl == configAct_m )
      ;
   else if ( sender_cl == helpAct_m )
      global::widgets::about()->exec();
   else
      assert( !"[ERROR] Incorrect IF brunch." );
}

void QTossWindow::slotPlayersSelectionsChanged( const QItemSelection & selected_cp, 
                                                const QItemSelection & /*deselected_cp*/ ) {
   const QModelIndexList selectedIndexes_cl = selected_cp.indexes();
   bool  playersGoupSelectionFound_l = false;
   for ( auto selectedIndex : selectedIndexes_cl ) 
      if ( !selectedIndex.parent().isValid() ) {
         playersTree_m->selectionModel()->select( selectedIndex, 
            QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows );
         playersGoupSelectionFound_l = true;
         break;
      }
   bool  playersSelectionFound_l = false,
         downPlayersFound_l = false,
         upPlayersFound_l = false;
   //
   if ( !playersGoupSelectionFound_l ) {
      const int   upIndex_cl = 0,
                  downIndex_cl = static_cast< int >( global::data::champ().groupsNames().size() 
                     ? global::data::champ().groupsNames().size() - 1 : 0 );
      //
      for( auto selectedIndex : playersTree_m->selectionModel()->selectedRows() )
         if ( selectedIndex.parent().isValid() ) {
            if ( selectedIndex.parent().row() > upIndex_cl && !downPlayersFound_l )
               downPlayersFound_l = true;
            if ( selectedIndex.parent().row() < downIndex_cl && !upPlayersFound_l )
               upPlayersFound_l = true;
            if ( !playersSelectionFound_l )
               playersSelectionFound_l = true;
         } 
   } // if ( !playersGoupSelectionFound_l ) {
   //
   addPlayerBtn_m->setEnabled( playersGoupSelectionFound_l );
   removePlayersBtn_m->setEnabled( playersSelectionFound_l );
   movePlayerUpBtn_m->setEnabled( downPlayersFound_l );
   movePlayerDownBtn_m->setEnabled( upPlayersFound_l );
}

void QTossWindow::slotNationsSelectionsChanged() {
   removeNationsBtn_m->setEnabled( nationsList_m->selectionModel()->selectedIndexes().size() );
}

void QTossWindow::slotScaleDiagram( int percents_p ) {
   const qreal scale_cl = static_cast< qreal >( percents_p ) / currentScalePercents_m;
   currentScalePercents_m = percents_p;
   toosResultsWidget_m->scale( scale_cl, scale_cl );
}

void QTossWindow::slotUpdateScene() {
   diagramScene_m->updateDiagram(); 
   checkStartButtonEnabled();
}

void QTossWindow::updateGameResultOnScene ( championship::size_type gameId_p ) {
   diagramScene_m->updateGameResult( gameId_p );
   checkStartButtonEnabled();
}

const QString & QTossWindow::windowTitleWithFile () {
   static const QString text_scl = global::consts::txt::tr( "���������� �� ��������� �� ����� " );
   return text_scl;
}

const QString & QTossWindow::windowTitleWithoutFile () {
   static const QString text_scl = global::consts::txt::tr( "����� �� ����������� ���������" );
   return text_scl;
}

