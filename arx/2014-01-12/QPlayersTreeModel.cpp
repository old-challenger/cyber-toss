#include <QPlayersTreeModel.h>
#include <championship.h>
#include <globals.h>

QVariant QPlayersTreeModel::data ( const QModelIndex & index_p, int role_p = Qt::DisplayRole ) const {
   if ( !index_p.isValid() )
      return QVariant();
   switch ( role_p ) {
      case Qt::DisplayRole: {
         const auto row_cl = static_cast< std::size_t >( index_p.row() );
         if( index_p.parent().isValid() ) {
            const std::size_t parentRow_cl = static_cast< std::size_t >( index_p.parent().row() );
            assert( index_pair::NO_PARENT == index_m.at( parentRow_cl ).parent() );
            auto players_cl = global::data::champ().players().find( index_m.at( parentRow_cl ).id() );
            assert( global::data::champ().players().end() != players_cl );
            //assert( row_cl < global::data::champ().players().at( index_m.at( parentRow_cl ).id() ).size() );
            //const std::size_t indexNumber_cl = index_m.at( parentRow_cl ).id() + row_cl;
            assert( parentRow_cl == index_m.at( row_cl ).parent() );
            auto player_cl = players_cl->second.find( index_m.at( row_cl ).id() );
            assert( players_cl->second.end() != player_cl );
            return index_p.column() ? player_cl->second.clan() : player_cl->second.nickname(); 
         } else {
            assert( index_pair::NO_PARENT == index_m.at( row_cl ).parent() );
            auto group_cl = global::data::champ().groupsNames().find( index_m.at( row_cl ).id() );
            assert( global::data::champ().groupsNames().end() != group_cl );
            return index_p.column() ? QVariant() : group_cl->second;
         }
      }
      default: return QVariant();
   }  // switch ( role_p ) {
}

//bool QPlayersTreeModel::hasChildren( const QModelIndex & parent_cp ) const {
//   if ( !parent_cp.isValid() )
//      return true;
//   const std::size_t row_cl = parent_cp.row();
//   assert( row_cl < index_m.size() );
//   if ( index_pair::NO_PARENT != index_m.at( row_cl ).parent() )
//      return false;
//   auto group_cl = global::data::champ().players().find( index_m.at( row_cl ).id() );
//   assert( global::data::champ().players().end() != group_cl );
//   return group_cl->second.size();
//}

QModelIndex QPlayersTreeModel::index ( int row_p, int column_p, const QModelIndex & parent_p ) const {
   const std::size_t row_cl = static_cast< std::size_t >( row_p );
   if( row_cl >= index_m.size() )
      return QModelIndex();
   if ( parent_p.isValid() ) {
      const auto parentRow_cl = static_cast< std::size_t >( parent_p.row() );
      assert( index_pair::NO_PARENT == index_m.at( parentRow_cl ).parent() );
      //const auto indexNumber_cl = parentRow_cl + row_cl;
      //assert( index_m.at( parentRow_cl ).id() == index_m.at( indexNumber_cl ).parent() );
      return createIndex( row_cl, column_p, ( void * ) &index_m.at( row_cl ) );
   } 
   if ( index_pair::NO_PARENT == index_m.at( row_cl ).parent() )
      return createIndex( row_p, column_p );
   return createIndex( row_p, column_p, ( void * ) &index_m.at( row_cl ) );
}

QVariant QPlayersTreeModel::headerData( int section_p, Qt::Orientation orientation_p, 
                                          int role_p = Qt::DisplayRole ) const {
   switch ( role_p ) {
      case Qt::DisplayRole: return headers().at( section_p );
      default: return QVariant();
   }
}

QModelIndex QPlayersTreeModel::parent( const QModelIndex & index_p ) const {
   if ( !index_p.isValid() )
      return QModelIndex();
   const std::size_t row_cl = index_p.row();
   const index_pair * parent_cl = ( index_pair * ) index_p.internalPointer();
   assert( nullptr == parent_cl || parent_cl->id() < index_m.size() );
   return ( nullptr == parent_cl )
      ? ( index_pair::NO_PARENT == index_m.at( row_cl ).parent() 
            ? QModelIndex() : createIndex( index_m.at( row_cl ).parent(), index_p.column() ) )
      : createIndex( parent_cl->parent(), index_p.column() );
}

void QPlayersTreeModel::rebuildIndex () {
   index_m.clear();
   std::size_t groupNumber_l;
   auto groups_cl = global::data::champ().players();
   for ( auto group_it = groups_cl.begin(); groups_cl.end() != group_it; ++group_it ) {
      groupNumber_l = index_m.size();
      index_m.push_back( index_pair( group_it->first, index_pair::NO_PARENT ) );
      for ( auto player_it = group_it->second.begin(); group_it->second.end() != player_it; ++player_it )
         index_m.push_back( index_pair( player_it->first, groupNumber_l ) );
   }
   //resetInternalData();
   emit dataChanged( createIndex( 0, 0 ), createIndex( index_m.size() - 1, columnCount() ) );
}

int QPlayersTreeModel::rowCount( const QModelIndex & parent_p ) const {
   // ��� ������ �������� ������
   if( !parent_p.isValid() )
      return index_m.size();
   // � ������ ����������� �������� ������ ������� ������
   //if( !parent_p.parent().isValid() ) 
   //   return 0;
   // ��� �������� ������� ������
   const std::size_t parentRow_cl = static_cast< std::size_t >( parent_p.row() );
   assert( parentRow_cl < index_m.size() );
   return index_pair::NO_PARENT == index_m.at( parentRow_cl ).parent()
      ? static_cast<int>( global::data::champ().players().at( index_m.at( parentRow_cl ).id() ).size() ) : 0;
}

std::vector< QString > QPlayersTreeModel::createHeadersVector () {
   std::vector< QString > result_l;
   result_l.reserve( 2 );
   result_l.push_back( global::consts::txt::codec()->toUnicode( "���������" ) );
   result_l.push_back( global::consts::txt::codec()->toUnicode( "����" ) );
   return result_l;
}

