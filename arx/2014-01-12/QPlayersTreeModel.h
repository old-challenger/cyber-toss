#ifndef QT_PLAYERS_TREE_MODEL_H_MRV
#define QT_PLAYERS_TREE_MODEL_H_MRV
//
#include <QAbstractItemModel>
//
#include <vector>
#include <cassert>

class QPlayersTreeModel : public QAbstractItemModel {
public:
   enum FILEDS {
      NICKNAME = 0,
      CLAN     = 1
   };

   static const std::vector< QString > & headers () {  
      static const std::vector< QString > headers_scl = createHeadersVector();
      return headers_scl;
   }

   QPlayersTreeModel ( QObject * parent_p = nullptr ) : QAbstractItemModel( parent_p ) {
      rebuildIndex();
   }
   
   int columnCount ( const QModelIndex & parent_cp = QModelIndex() ) const 
   {  return 2; }

   QVariant data ( const QModelIndex & index_cp, int role_p ) const;

   Qt::ItemFlags flags ( const QModelIndex & /*index_cp*/ ) const { 
      return Qt::ItemIsSelectable | Qt::ItemIsEnabled; 
   }

   bool hasChildren( const QModelIndex & parent_cp = QModelIndex() ) const;
   
   QModelIndex index ( int row_p, int column_p, const QModelIndex & parent_cp = QModelIndex() ) const;
   
   QVariant headerData ( int section_p, Qt::Orientation orientation_p, int role_p ) const;

   std::size_t getGroupId ( const QModelIndex & index_cp ) {
      assert( !index_cp.parent().isValid() );
      const auto row_cl = static_cast< std::size_t >( index_cp.row() );
      assert( row_cl < index_m.size() && index_pair::NO_PARENT == index_m.at( row_cl ).parent() );
      return index_m.at( row_cl ).id();
   }
   
   QModelIndex parent ( const QModelIndex & index_cp ) const;
   
   void rebuildIndex ();
   
   int rowCount( const QModelIndex & parent_cp = QModelIndex() ) const;
private:

   class index_pair {
   public:
      enum { NO_PARENT = -1 };
      
      index_pair ( std::size_t id_p, std::size_t parent_p ) : id_m( id_p ), parent_m( parent_p ) { ; }
      index_pair ( const index_pair & etalon_cp ) : id_m( etalon_cp.id_m ), parent_m( etalon_cp.parent_m ) { ; }

      std::size_t id () const { return id_m; }
      std::size_t parent () const { return parent_m; }
   public:
      std::size_t id_m,
                  parent_m;
   }; // class index_pair {

   std::vector< index_pair > index_m;

   static std::vector< QString > createHeadersVector ();
}; // class QPlayersTreeModel 
#endif // QT_PLAYERS_TREE_MODEL_H_MRV