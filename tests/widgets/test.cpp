#include <QTossProcessWindow.h>
#include <QApplication>

int main ( int argc_p, char ** argv_p ) {
   QApplication app_l( argc_p, argv_p );
   QTossProcessWindow * const wgt_l = new QTossProcessWindow();
   wgt_l->show();
   return app_l.exec();
}