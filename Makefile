
MAKEFILE      = Makefile

CXXFLAGS    = -c -std=c++11 -pipe -fno-keep-inline-dllexport -O2 -frtti -Wall -Wextra -fexceptions -mthreads
DEFINES     = -DQT_NO_DEBUG -DQT_WIDGETS_LIB -DQT_GUI_LIB -DQT_CORE_LIB -DQT_NEEDS_QMAIN
LFLAGS      = -Wl,-s -Wl,-subsystem,windows -mthreads
INCPATH     = -I".\include" -I"$(QTDIR)\include" -I"$(QTDIR)\include\QtWidgets" -I"$(QTDIR)\include\QtCore" -I"$(QTDIR)\include\QtGui" -I"$(QTDIR)\include\QtXml" -I"$(MYLIB)\c++\qt" -I"$(MYLIB)\c++\utl"
LIBS        = -lglu32 -lgdi32 -luser32 -lmingw32 -L"$(QTDIR)\lib\mingw\x86" -lqtmain -lQt5Widgets -lQt5Gui -lQt5Core -lQt5Xml
SOURCES     = .\src
HEADERS     = .\include
MOCS        = .\moc

OBJECTS_DIR = $(OBJDIR)\cyber-toss\mingw
OUTPUT_DIR  = $(TESTBIN)\cyber-toss\mingw

all: mocing compiling linking

mocing:
	moc $(HEADERS)\QChooseWinnerDialog.h -o $(MOCS)\moc_QChooseWinnerDialog.cpp
	moc $(HEADERS)\QGameWidget.h -o $(MOCS)\moc_QGameWidget.cpp
	moc $(HEADERS)\QTossProcessWindow.h -o $(MOCS)\moc_QTossProcessWindow.cpp
	moc $(HEADERS)\QTossThread.h -o $(MOCS)\moc_QTossThread.cpp
	moc $(HEADERS)\QTossWindow.h -o $(MOCS)\moc_QTossWindow.cpp

compiling:
	if not exist $(OBJECTS_DIR) mkdir $(OBJECTS_DIR)
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\championship.o $(SOURCES)\championship.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\main.o $(SOURCES)\main.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QChooseNationDialog.o $(SOURCES)\QChooseNationDialog.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QChooseWinnerDialog.o $(SOURCES)\QChooseWinnerDialog.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QDiagramScene.o $(SOURCES)\QDiagramScene.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QGameWidget.o $(SOURCES)\QGameWidget.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QNationsListModel.o $(SOURCES)\QNationsListModel.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QPlayersTreeModel.o $(SOURCES)\QPlayersTreeModel.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QStringEnterDialog.o $(SOURCES)\QStringEnterDialog.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QTossProcessWindow.o $(SOURCES)\QTossProcessWindow.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QTossThread.o $(SOURCES)\QTossThread.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\QTossWindow.o $(SOURCES)\QTossWindow.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\moc_QChooseWinnerDialog.o $(MOCS)\moc_QChooseWinnerDialog.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\moc_QGameWidget.o $(MOCS)\moc_QGameWidget.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\moc_QTossProcessWindow.o $(MOCS)\moc_QTossProcessWindow.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\moc_QTossThread.o $(MOCS)\moc_QTossThread.cpp
	g++ $(CXXFLAGS) $(DEFINES) $(INCPATH) -o $(OBJECTS_DIR)\moc_QTossWindow.o $(MOCS)\moc_QTossWindow.cpp

linking:
	if not exist $(OUTPUT_DIR) mkdir $(OUTPUT_DIR)
	g++ $(LFLAGS) -o $(OUTPUT_DIR)\cyber-toss.exe $(OBJECTS_DIR)\championship.o $(OBJECTS_DIR)\QChooseNationDialog.o $(OBJECTS_DIR)\QChooseWinnerDialog.o $(OBJECTS_DIR)\main.o $(OBJECTS_DIR)\QDiagramScene.o $(OBJECTS_DIR)\QGameWidget.o $(OBJECTS_DIR)\QNationsListModel.o $(OBJECTS_DIR)\QPlayersTreeModel.o $(OBJECTS_DIR)\QStringEnterDialog.o $(OBJECTS_DIR)\QTossProcessWindow.o $(OBJECTS_DIR)\QTossThread.o $(OBJECTS_DIR)\QTossWindow.o $(OBJECTS_DIR)\moc_QChooseWinnerDialog.o $(OBJECTS_DIR)\moc_QGameWidget.o $(OBJECTS_DIR)\moc_QTossProcessWindow.o $(OBJECTS_DIR)\moc_QTossThread.o $(OBJECTS_DIR)\moc_QTossWindow.o $(LIBS)
   

clean: 
	del $(MOCS)\moc_QChooseWinnerDialog.cpp
	del $(MOCS)\moc_QGameWidget.cpp
	del $(MOCS)\moc_QTossProcessWindow.cpp
	del $(MOCS)\moc_QTossThread.cpp
	del $(MOCS)\moc_QTossWindow.cpp

	del $(OBJECTS_DIR)\championship.o
	del $(OBJECTS_DIR)\main.o
	del $(OBJECTS_DIR)\QChooseNationDialog.o
	del $(OBJECTS_DIR)\QChooseWinnerDialog.o
	del $(OBJECTS_DIR)\QDiagramScene.o
	del $(OBJECTS_DIR)\QGameWidget.o
	del $(OBJECTS_DIR)\QNationsListModel.o
	del $(OBJECTS_DIR)\QPlayersTreeModel.o
	del $(OBJECTS_DIR)\QStringEnterDialog.o
	del $(OBJECTS_DIR)\QTossProcessWindow.o
	del $(OBJECTS_DIR)\QTossThread.o
	del $(OBJECTS_DIR)\QTossWindow.o
	del $(OBJECTS_DIR)\moc_QChooseWinnerDialog.o
	del $(OBJECTS_DIR)\moc_QGameWidget.o
	del $(OBJECTS_DIR)\moc_QTossProcessWindow.o
	del $(OBJECTS_DIR)\moc_QTossThread.o
	del $(OBJECTS_DIR)\moc_QTossWindow.o

	del $(OUTPUT_DIR)\cyber-toss.exe