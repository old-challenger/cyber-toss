#ifndef QT_NATIONS_LIST_MODEL
#define QT_NATIONS_LIST_MODEL

#include <QAbstractListModel>
#include <vector>

class QNationsListModel : public QAbstractListModel {
public:
   static std::vector< QString > createHeaderVector ();

   QNationsListModel ( QObject * parent_p = nullptr ) : QAbstractListModel( parent_p ) { ; }

   void add ( int nationId_p );

   QModelIndex index ( int row_p, int column_p = 0, const QModelIndex & parent_cp = QModelIndex() ) const;

   Qt::ItemFlags flags ( const QModelIndex & /*index_cp*/ ) const { 
      return Qt::ItemIsSelectable | Qt::ItemIsEnabled; 
   }

   QVariant data ( const QModelIndex & index_cp, int role_p = Qt::DisplayRole ) const;

   void remove( const QModelIndexList & indexes_cp );

   int rowCount( const QModelIndex & parent_cp = QModelIndex() ) const;
}; // class QNationsListModel
#endif // QT_NATIONS_LIST_MODEL