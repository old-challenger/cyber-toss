#ifndef QT_CHOOSE_WINNER_DIALOG_H_MRV
#define QT_CHOOSE_WINNER_DIALOG_H_MRV

#include <championship.h>
#include <QDialog>

class QLabel;
class QPushButton;
class QRadioButton;

class QChooseWinnerDialog : public QDialog {
   Q_OBJECT
private:
   QLabel * gameLbl_m;
   QPushButton * okBtn_m;
   QRadioButton   * favorite_m,
                  * outsider_m;
   
   static QString playerPlusNation ( const QString & player_cp, const QString & nation_cp ) {
      static const QString newLine_scl = QLatin1String( "\n" );
      return player_cp + newLine_scl + nation_cp;
   }
private slots:
   
   void slotActivateOkBtn ();
public:
   QChooseWinnerDialog ();

   championship::round::game::result_enum execChoosing ();
   
   void setFavorite ( const QString & player_cp, const QString & nation_cp );

   void setGame ( const QString & gameFullName_cp );

   void setGameResult ( championship::round::game::result_enum result_p );

   void setOutsider ( const QString & player_cp, const QString & nation_cp );
}; // class QChooseWinnerDialog

#endif // QT_CHOOSE_WINNER_DIALOG_H_MRV