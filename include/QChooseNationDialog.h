#ifndef QT_CHOOSE_FROM_LIST_DIALOG_H_MRV
#define QT_CHOOSE_FROM_LIST_DIALOG_H_MRV

#include <QComboBox>
#include <QDialog>

class QChooseNationDialog : public QDialog {
public:

   QChooseNationDialog ();

   int currentId() const { 
      return comboBox_m->itemData( comboBox_m->currentIndex() ).toInt(); 
   }

   int exec(); 
private:
   QComboBox * comboBox_m;
}; // class QChooseNationDialog
#endif // QT_CHOOSE_FROM_LIST_DIALOG_H_MRV
