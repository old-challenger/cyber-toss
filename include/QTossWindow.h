#ifndef QT_TOSS_WINDOW_H_MRV
#define QT_TOSS_WINDOW_H_MRV
//
#include <QPlayersTreeModel.h>
#include <QNationsListModel.h>
#include <QDiagramScene.h>
//
#include <QGraphicsView>
#include <QMainWindow>
#include <QToolButton>
#include <QTreeView>
#include <QListView>
#include <QAction>
#include <QLabel>
//

class QTossWindow : public QMainWindow {
   Q_OBJECT
public:

   QTossWindow ();

   void updateGameResultOnScene ( championship::size_type gameId_p );
private:
   QGraphicsView  * toosResultsWidget_m;

   QTreeView * playersTree_m;
   QListView * nationsList_m;

   QPlayersTreeModel * playersModel_m;
   QNationsListModel * nationsModel_m;
   QDiagramScene * diagramScene_m;

   QToolButton * addPlayerBtn_m,
               * removePlayersBtn_m,
               * movePlayerUpBtn_m,
               * movePlayerDownBtn_m,
               * addNationBtn_m,
               * removeNationsBtn_m;

   QLabel * scaleLbl_m;

   QAction  * newTossAct_m,
            * openTossAct_m,
            * saveTossAct_m,
            * saveTossAsAct_m,
            * exitAct_m,
            * startAct_m,
            * printAct_m,
            * configAct_m,
            * helpAct_m;

   int currentScalePercents_m;

   static const QString & windowTitleWithFile ();
   static const QString & windowTitleWithoutFile ();

   void allDataWereChanged ();
   void checkStartButtonEnabled();
   QFrame * createPlayersWidget ();
      QFrame * createNationsWidget ();
   void createMenu ();
   void createToolBar ();
   QString execSaveAsDialog ();
   QString makeMoveElementsText ( int elementsListSize_p );
   void saveDataToFile ( const QString fileName_cp = QString() );
private slots:
   void slotNationsActions ();
   void slotMenuActions    ();
   void slotPlayersActions ();
   void slotPlayersSelectionsChanged ( const QItemSelection & selected_cp, const QItemSelection & /*deselected_cp*/ );
   void slotNationsSelectionsChanged ();
   void slotScaleDiagram   ( int percents_p );
   void slotUpdateScene    ();
}; // class QTossWindow 
//
#endif //QT_TOSS_WINDOW_H_MRV