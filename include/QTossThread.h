#ifndef QT_TOSS_THREAD_H_MRV
#define QT_TOSS_THREAD_H_MRV
//
#include <QThread>
#include <QMap>

#include <championship.h>

class QTossThread : public QThread {
   Q_OBJECT
public:
   enum textValueTypes_enum {
      FAVORITE_PLAYERS_NAME   = 1,
      FAVORITE_PLAYERS_NATION = 2,
      OUTSIDER_PLAYERS_NAME   = 3,
      OUTSIDER_PLAYERS_NATION = 4,
   };

   QTossThread () : QThread (), notStopFlag_m( true ) { ; }
public slots:
   
   void start () {
      notStopFlag_m = true;
      QThread::start();
   }

   void sltStop () { notStopFlag_m = false; }

signals:

   void nextGame ( int gameNumber_p );

   void nextTxtValue ( int textValueType_p, QString textValue_cp );
   
   void tossStarted ( int tourDenominator_p );

   void championshipDataUpdated ();
protected:

   void run ();
private:
   typedef QMap< championship::size_type, championship::ids_set > maped_sets;

   static const int tossIterationsAmount_scm = 7;

   static const unsigned long int   cleansFieldDelay_scm = 1, //100,
                                    fillFiledDelay_scm   = 3, //300,
                                    nextGameDelay_scm    = 5; //500;

   bool notStopFlag_m;

   int chooseNextNationId ( textValueTypes_enum textValueType_p );

   int chooseNextPlayerId ( textValueTypes_enum textValueType_p, 
      const championship::round::roundsPlayers_map::const_iterator & playersGroup_cp,
      maped_sets & chosenPlayers_rp );
}; // class QTossThread


#endif //QT_TOSS_THREAD_H_MRV