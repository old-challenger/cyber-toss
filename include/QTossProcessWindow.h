#ifndef QT_TOSS_PROCESS_WINDOW_H_MRV
#define QT_TOSS_PROCESS_WINDOW_H_MRV

#include <QFrame>
#include <QLabel>

#include <QTossThread.h>

class QTossProcessWindow : public QFrame {
   Q_OBJECT
public:

   QTossProcessWindow ();

private:
   QLabel   * gameNumber_m,
            * favoritePlayer_m,
            * favoriteNation_m,
            * outsiderPlayer_m,
            * outsiderNation_m;
private slots:

   void sltClean ();

   void sltSetGameNumber ( int number_p );

   void sltSetTxtValue ( int textValueType_p, QString textValue_p );

   //void sltSetFavoritePlayer ( QString favoritePlayer_cp ) {
   //   favoritePlayer_m->setText( favoritePlayer_cp );
   //}

   //void sltSetFavoritePlayer ( int favoriteNationId_p ) {
   //   favoriteNation_m->setText( nationTemplate().arg( QString::number( favoriteNationId_p ) ) );
   //}

   //void sltSetOutsiderPlayer ( QString outsiderPlayer_cp ) {
   //   outsiderPlayer_m->setText( outsiderPlayer_cp );
   //}

   //void sltSetOutsiderPlayer ( int outsiderNationId_p ) {
   //   outsiderNation_m->setText( nationTemplate().arg( QString::number( outsiderNationId_p ) ) );
   //}

   void sltSetWindowTitle ( int tourDenominator_p );
};
#endif // QT_TOSS_PROCESS_WINDOW_H_MRV