#ifndef GLOBALS_H_MRV
#define GLOBALS_H_MRV
//
#include <QChooseWinnerDialog.h>
#include <QChooseNationDialog.h>
#include <QStringEnterDialog.h>
#include <QTossProcessWindow.h>
#include <championship.h>
#include <QTossWindow.h>
#include <QTossThread.h>
#include <pics.h>
//
#include <QStringList>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextCodec>
#include <QString>
#include <QDir>
//
#include <map>
//
namespace global {
   
namespace consts {
   
   class txt {
   private:
      txt () { ; }

      static const QString & siffix0 () {
         static const QString txt_scl = global::consts::txt::tr( "�" );
         return txt_scl;
      }

      static const QString & siffix1 () {
         static const QString txt_scl = global::consts::txt::tr( "��" );
         return txt_scl;
      }
   public:
      static QTextCodec * codec () {
         static QTextCodec * const codec_scl = QTextCodec::codecForName( "windows-1251" );
         return codec_scl;
      }

      static const QString tr( const char * ascii_cp ) { 
         return codec()->toUnicode( ascii_cp );
      }

      static const QString & add () {
         static const QString txt_scl = tr( "��������" );
         return txt_scl;
      }

      static const QString & cancel () {
         static const QString txt_scl = tr( "��������" );
         return txt_scl;
      }

      static const QString & check () {
         static const QString txt_scl = tr( "�������" );
         return txt_scl;
      }

      static const QString & dot () {
         static const QString txt_scl = QLatin1String( "." );
         return txt_scl;
      }

      static const QString & element () {
         static const QString txt_scl = tr( "�������" );
         return txt_scl;
      }

      template < class size_type >
      static QString elements ( size_type elementsSize_p ) {
         return element() + ( 1 == elementsSize_p 
            ? QString() : 1 < elementsSize_p && elementsSize_p < 5 ? siffix0() : siffix1() );
      }

      static const QString & fileType () {
         static const QString txt_scl = QLatin1String( "CyberTossSaveFile-v.0.1" );
         return txt_scl;
      }

      static QString gameComplexName ( int roundDelimeter_p, int gameNumber_p ) {
         if ( 1 == roundDelimeter_p ) {
            return roundsName( roundDelimeter_p );
         }
         static const QString txt_scl = tr( " - ���� �%1" );
         return roundsName( roundDelimeter_p ) + txt_scl.arg( gameNumber_p + 1 );
      }

      static const QString & nationTemplate () {
         static const QString text_scl = QLatin1String( "(%1)" );
         return text_scl;
      }

      static QString percents ( int value_p ) {
         static const QString txt_scl = QLatin1String( "%1 %" );
         return txt_scl.arg( value_p );
      }

      static QString playerComplexName ( const QString & clan_cp, const QString & name_cp ) {
         static const QString txt_scl = QLatin1String( "[%1]%2" );
         return clan_cp.isEmpty() ? name_cp : txt_scl.arg( clan_cp ).arg( name_cp );
      }

      static const QString & remove () {
         static const QString txt_scl = tr( "������" );
         return txt_scl;
      }

      static const QString roundsName ( int roundDelimeter_p ) {
         if ( 1 == roundDelimeter_p ) {
            static const QString finalTxt_scl = tr( "�����" );
            return finalTxt_scl;
         }
         static const QString txt_scl = tr( "1/%1 ������" );
         return txt_scl.arg( roundDelimeter_p );
      }

      static const QString & setDeleted () {
         static const QString txt_scl = global::consts::txt::tr( " ������� " );
         return txt_scl;
      }

      static const QString & setSelected () {
         static const QString txt_scl = global::consts::txt::tr( " ���������� " );
         return txt_scl;
      }
      static const QString & singleDeleted () {
         static const QString txt_scl = global::consts::txt::tr( " ����� " );
         return txt_scl;
      }

      static const QString & singleSelected () {
         static const QString txt_scl = global::consts::txt::tr( " ���������� " );
         return txt_scl;
      }

      static const QString & space () {
         static const QString txt_scl = QLatin1String( " " );
         return txt_scl;
      }

      static const QString & willBe () {
         static const QString txt_scl = global::consts::txt::tr( "�����" );
         return txt_scl;
      }
   }; // class txt {

   class icons {
   public:
      static const QIcon & check () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::check24() );
         return icon_scl;
      }

      static const QIcon & close () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::close24() );
         return icon_scl;
      }

      static const QIcon & config () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::config24() );
         return icon_scl;
      }

      static const QIcon & create () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::create24() );
         return icon_scl;
      }

      static const QIcon & cross () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::cross24() );
         return icon_scl;
      }

      static const QIcon & down () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::down24() );
         return icon_scl;
      }

      static const QIcon & open () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::open24() );
         return icon_scl;
      }

      static const QIcon & plus () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::plus24() );
         return icon_scl;
      }

      static const QIcon & print () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::print24() );
         return icon_scl;
      }

      static const QIcon & save () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::save24() );
         return icon_scl;
      }

      static const QIcon & saveAs () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::saveAs24() );
         return icon_scl;
      }

      static const QIcon & start () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::start24() );
         return icon_scl;
      }

      static const QIcon & toss () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::toss16(), pics::toss32(), pics::toss48() );
         return icon_scl;
      }

      static const QIcon & up () {
         static const QIcon icon_scl = utl::icon::createIcon( pics::up24() );
         return icon_scl;
      }
   private: 
      icons () { ; }
   }; // class icons {
}; // namespace consts {

class data {
public:
   static championship & champ () {
      static championship single_scl;
      return single_scl;
   }
private:
   data () { ; }
}; // class data {

class objects {
public:
   static QTossThread * tossThread () {
      static QTossThread * thread_sl = new QTossThread();
      return thread_sl;
   }
private:
   objects () { ; }
};

class widgets {
public:
   static QMessageBox * about () {
      static QMessageBox * const widget_sl = createAboutDialog();
      return widget_sl;
   }

   static QChooseNationDialog * chooseNation () {
      static QChooseNationDialog * const widget_sl = createChooseNationDialog();
      return widget_sl;
   }

   static QChooseWinnerDialog * chooseWinner () {
      static QChooseWinnerDialog * const widget_sl = new QChooseWinnerDialog();
      return widget_sl;
   }

   static QStringEnterDialog * enterPlayerStsrings () {
      static QStringEnterDialog * const widget_sl = new QStringEnterDialog( QPlayersTreeModel::headers() );
      return widget_sl;
   }

   static QMessageBox * information () {
      static QMessageBox * const widget_l = createInformation();
      return widget_l;
   }

   static QFileDialog * fileDialog () {
      static QFileDialog * const widget_sl = createFileDialog();
      return widget_sl;
   }

   static QTossWindow * mainWindow () {
      static QTossWindow * const widget_sl = new QTossWindow();
      return widget_sl;
   }

   static QMessageBox * question () {
      static QMessageBox * const widget_l = createQuestion();
      return widget_l;
   }

   static QTossProcessWindow * tossProcess () {
      static QTossProcessWindow * const widget_l = new QTossProcessWindow();
      return widget_l;
   }
private:

   widgets () { ; }

   static QMessageBox * createAboutDialog () {
      QMessageBox * const widget_l = new QMessageBox( QMessageBox::Icon::NoIcon, 
         consts::txt::tr( "���������� � ��������� Cyber Toss" ), 
         consts::txt::tr( "��� ����� ���������� � ���������" ), 
         QMessageBox::Ok );
      widget_l->setWindowIcon( consts::icons::toss() );
      widget_l->setIconPixmap( utl::icon::createPixmap( pics::clan128() ) );
      widget_l->setTextFormat( Qt::TextFormat::RichText );
      widget_l->setText( consts::txt::tr( "<p><b>Cyber Toss</b></p><p><i>������ 0.1</i><p>" 
         "<p>��������� ��� ���������� ���������� ��������������� ����������� �� ���������� ����� \"������\""
         " � \"���������� ������\".</p><p><table cellspacing=\"7\"><tbody>"
         "<tr><td>����� ����:</td><td><a href=\"mailto:bigza@yandex.ru\">�������� ������</a></td></tr>"
         "<tr><td>�����������:</td><td><a href=\"mailto:mrv.work.box@yandex.ru\">������ �. �������</a></td></tr>"
         //"<tr><td>���������� �� �������:</td><td><a href=\"mailto:no@mail.ru\">����� �. �������</a></td></tr>"
         "</tbody></table></p><p>�������������� �����������:<ul>"
         "<li><a href=\"http://www.glc-corp.com/\">����</a> �������� ����� Gloomy Legion of Chaos (GLC);</li>"
         "<li>������ ���� \"���������� �������\"<a href=\"https://vk.com/glccorp\"> ���������</a>;</li>"
         "<li>������ ���� \"���������� �������\" � <a href=\"https://www.facebook.com/groups/736529689708061/\">Facebook</a>.</li>"
         "</ul></p>" ) );
      assert( 1 == widget_l->buttons().size() );
      QPushButton * const okBtn_l = dynamic_cast< QPushButton * >( widget_l->button( QMessageBox::Ok ) );
      assert( nullptr != okBtn_l );
      okBtn_l->setText( consts::txt::tr( "�������" ) );
      okBtn_l->setIcon( consts::icons::check() );
      return widget_l;
   }

   static QChooseNationDialog * createChooseNationDialog () {
      QChooseNationDialog * const widget_l = new QChooseNationDialog();
      widget_l->setWindowIcon( consts::icons::toss() );
      const QString title_cl = consts::txt::tr( "�������� ����� ��� ���������� � ������" );
      widget_l->setWindowTitle( title_cl );
      const QFontMetrics metrics_cl( widget_l->font() );
      widget_l->setMinimumWidth( metrics_cl.width( title_cl ) * 1.333 );
      return widget_l;
   }

   static QMessageBox * createInformation () {
      QMessageBox * const widget_l = new QMessageBox( QMessageBox::Icon::Information, 
         consts::txt::tr( "��������� ��������" ), QString(), QMessageBox::Ok );
      widget_l->setWindowIcon( consts::icons::toss() );
      assert( 1 == widget_l->buttons().size() );
      QPushButton * const okBtn_l = dynamic_cast< QPushButton * >( widget_l->button( QMessageBox::Ok ) );
      assert( nullptr != okBtn_l );
      okBtn_l->setText( consts::txt::tr( "�������" ) );
      okBtn_l->setIcon( consts::icons::check() );
      return widget_l;
   }

   static QFileDialog * createFileDialog () {
      static const QString  extention_scl = QLatin1String( " (*.cts)" );
      QFileDialog * const widget_l = new QFileDialog();
      widget_l->setDefaultSuffix( QLatin1String( "xml" ) );
      widget_l->setDirectory( QDir::currentPath() );
      widget_l->setNameFilters( QStringList() << consts::txt::fileType() + extention_scl
         << consts::txt::tr( "XML-����� (*.xml)" ) 
         << consts::txt::tr( "��� ����� (*)" ) );
      //widget_l-setLabelText( QFileDialog::LookIn, "" );
      widget_l->setLabelText( QFileDialog::FileName, consts::txt::tr( "��� �����" ) );
      widget_l->setLabelText( QFileDialog::FileType, consts::txt::tr( "��� �����" ) );
      widget_l->setLabelText( QFileDialog::Accept, consts::txt::check() );
      widget_l->setLabelText( QFileDialog::Reject, consts::txt::cancel() );
      return widget_l;
   }

   static QMessageBox * createQuestion () {
      QMessageBox * const widget_l = new QMessageBox( QMessageBox::Icon::Question, 
         consts::txt::tr( "������������� ��������" ), QString(), 
         QMessageBox::Ok | QMessageBox::Cancel );
      widget_l->setWindowIcon( consts::icons::toss() );
      widget_l->setDefaultButton( QMessageBox::Cancel );
      QPushButton * const okBtn_l = dynamic_cast< QPushButton * >( widget_l->button( QMessageBox::Ok ) ),
                  * const cancelBtn_l = dynamic_cast< QPushButton * >( widget_l->button( QMessageBox::Cancel ) );
      assert( nullptr != okBtn_l && nullptr != cancelBtn_l );
      okBtn_l->setIcon( consts::icons::check() );
      okBtn_l->setText( consts::txt::tr( "�����������" ) );
      cancelBtn_l->setIcon( consts::icons::cross() );
      cancelBtn_l->setText( consts::txt::cancel() );
      return widget_l;
   }
}; // class widgets {
}; // namespace globals {
//
#endif // GLOBALS_H_MRV