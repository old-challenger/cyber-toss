#include <globals.h>
#include <QApplication>

class QTossApplication : public QApplication {
public:
   QTossApplication ( int num_p, char ** args_p ) : QApplication( num_p, args_p ) {
      connect( global::objects::tossThread(), SIGNAL( started() ), 
         global::widgets::tossProcess(), SLOT( show() ) ); 
      connect( global::objects::tossThread(), SIGNAL( finished() ), 
         global::widgets::tossProcess(), SLOT( close() ) ); 
      connect( global::objects::tossThread(), SIGNAL( nextGame( int ) ), 
         global::widgets::tossProcess(), SLOT( sltSetGameNumber( int ) ) ); 
      connect( global::objects::tossThread(), SIGNAL( nextTxtValue( int, QString ) ), 
         global::widgets::tossProcess(), SLOT( sltSetTxtValue( int, QString ) ) ); 

      //connect( global::objects::tossThread(), SIGNAL( nextFavoritePlayer( int ) ), 
      //   global::widgets::tossProcess(), SLOT( sltSetFavoritePlayer( int ) ) ); 
      //connect( global::objects::tossThread(), SIGNAL( nextOutsiderPlayer( QString ) ), 
      //   global::widgets::tossProcess(), SLOT( sltSetOutsiderPlayer( QString ) ) ); 
      //connect( global::objects::tossThread(), SIGNAL( nextOutsiderPlayer( int ) ), 
      //   global::widgets::tossProcess(), SLOT( sltSetOutsiderPlayer( int ) ) ); 
      
      connect( global::objects::tossThread(), SIGNAL( tossStarted( int ) ), 
         global::widgets::tossProcess(), SLOT( sltSetWindowTitle( int ) ) ); 
      connect( global::objects::tossThread(), SIGNAL( championshipDataUpdated() ), 
         global::widgets::mainWindow(), SLOT( slotUpdateScene() ) );
   }

   ~ QTossApplication () {
      global::widgets::about()->deleteLater();
      global::widgets::chooseNation()->deleteLater();
      global::widgets::chooseWinner()->deleteLater();
      global::widgets::enterPlayerStsrings()->deleteLater();
      global::widgets::information()->deleteLater();
      global::widgets::fileDialog()->deleteLater();
      global::widgets::mainWindow()->deleteLater();
      global::widgets::question()->deleteLater();
      global::widgets::tossProcess()->deleteLater();
   }
};