#ifndef QT_STRING_ENTER_DIALOG_H_MRV
#define QT_STRING_ENTER_DIALOG_H_MRV

#include <QDialog>
#include <QString>
#include <QLineEdit>

#include <vector>
#include <cassert>

class QStringEnterDialog : public QDialog {
public:
   typedef std::vector< QString > string_vector;

   QStringEnterDialog ( const std::vector< QString > & fieldsHeaders_cp );

   QString getString ( std::size_t stringId_p ) const { 
      assert( stringId_p < stringFields_m.size() );
      return stringFields_m.at( stringId_p )->text(); 
   }

   int exec(); 
private:
   std::vector< QLineEdit * > stringFields_m;
};

#endif // QT_STRING_ENTER_DIALOG_H_MRV