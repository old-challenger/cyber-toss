#ifndef QT_DIAGRAM_SCENE_H_MRV
#define QT_DIAGRAM_SCENE_H_MRV

#include <championship.h>

#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QVector>
#include <QWidget>

class QDiagramScene : public QGraphicsScene {
public:
   QDiagramScene ( QObject * parent_p ) : QGraphicsScene( parent_p ), roundTitleHeight_m( 0 ), 
      roundSectionSize_m( 0, 0 ), maxLengthString_m(), roundsField_m(), roundsTitles_m(), gamesItems_m(), 
      playersItems_m(), gamesResultsIndex_m() { ; }

   void clearDiagram       ();
   void initDiagram        ( championship::size_type roundsAmount_p );
   void updateDiagram      ();
   void updateGameResult   ( championship::size_type gameId_p );
private:
   typedef QVector< QGraphicsItem * >                                items_vector;
   typedef QVector< QGraphicsProxyWidget * >                         roundItems_vector;
   typedef QVector< QVector< QGraphicsProxyWidget * > >              roundsItems_matrix;
   typedef QMap< championship::size_type, championship::size_type >  items_index;

   qreal                roundTitleHeight_m;
   QSizeF               roundSectionSize_m; 
   QString              maxLengthString_m;
   items_vector         roundsField_m,
                        roundsTitles_m;
   roundsItems_matrix   gamesItems_m;
   roundItems_vector    playersItems_m;
   items_index          gamesResultsIndex_m;
   
   void clearGraphItemsVector ( items_vector & items_rp ) {
      for ( auto item_it : items_rp ) {
         removeItem( item_it );
         delete item_it;
      }
      items_rp.clear();
   }

   void clearGraphWidgtesVector ( roundItems_vector & items_rp ) {
      for ( auto item_it : items_rp ) {
         removeItem( item_it->graphicsItem() );
         item_it->deleteLater();
      }
      items_rp.clear();
   }

   roundItems_vector::size_type addAndPoseWidget ( championship::size_type roundSectionId_p, 
      championship::size_type widgetIndex_p, QWidget * widget_p, roundItems_vector & itemsContainer_rp, 
      bool player_p = false );
   
   void checkMaxTextWidth ( const QString & text_cp, const QFontMetricsF & metrics_cp, 
      QSizeF & currentMaxSize_rp );

   QWidget * createPlayerWidget ( championship::size_type groupId_p, championship::size_type playerId_p, 
      const QString & maxLineText_cp );

   void removePlayersItems ();

   qreal roundSectionY ( championship::size_type roundNumber_p ) {
      return roundNumber_p * roundSectionSize_m.height();
   }
}; // class QDiagramScene : public QGraphicsScene {
#endif // QT_DIAGRAM_SCENE_H_MRV