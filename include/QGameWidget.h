#ifndef QT_GAME_WIDGET_H_MRV
#define QT_GAME_WIDGET_H_MRV

#include <championship.h>

#include <QString>
#include <QFrame>

class QLabel;

class QGameWidget : public QFrame {
   Q_OBJECT
public:
   QGameWidget( /*QWidget * parent_p, */championship::size_type roundId_p, championship::size_type gameId_p, 
      const QString & maxLineText_p );
private:
   championship::size_type roundId_m,
                           gameId_m;

   QLabel * game_m,
          * favoriteName_m,
          * favoriteNation_m,
          * outsiderName_m,
          * outsiderNation_m;

   QLabel * createLabel ( const QString & text_cl );

   void mousePressEvent( QMouseEvent * event_p );
};
#endif //QT_GAME_WIDGET_H_MRV

