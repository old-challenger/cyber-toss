#ifndef QT_PLAYERS_TREE_MODEL_H_MRV
#define QT_PLAYERS_TREE_MODEL_H_MRV
//
#include <championship.h>
//
#include <QAbstractItemModel>
#include <QVector>
#include <QSet>
//
#include <cassert>

class QPlayersTreeModel : public QAbstractItemModel {
public:
   enum FILEDS {
      NICKNAME = 0,
      CLAN     = 1
   };

   static const std::vector< QString > & headers () {  
      static const std::vector< QString > headers_scl = createHeadersVector();
      return headers_scl;
   }

   QPlayersTreeModel ( QObject * parent_p = nullptr ) : QAbstractItemModel( parent_p ) { rebuildIndex(); }
   
   void add ( const QModelIndex & groupIndex_cp, const QString & nickname_cp, const QString & clanname_cp );

   int columnCount ( const QModelIndex & parent_cp = QModelIndex() ) const 
   {  return 3; }

   QVariant data ( const QModelIndex & index_cp, int role_p ) const;

   Qt::ItemFlags flags ( const QModelIndex & /*index_cp*/ ) const { 
      return Qt::ItemIsSelectable | Qt::ItemIsEnabled; 
   }

   QModelIndex index ( int row_p, int column_p, const QModelIndex & parent_cp = QModelIndex() ) const;
   
   QVariant headerData ( int section_p, Qt::Orientation orientation_p, int role_p ) const;

   void moveDown ( const QModelIndexList & indexes_cp );

   void moveUp ( const QModelIndexList & indexes_cp );

   QModelIndex parent ( const QModelIndex & index_cp ) const;

   void rebuildIndex();

   void remove( const QModelIndexList & indexes_cp );
   
   int rowCount( const QModelIndex & parent_cp = QModelIndex() ) const;
private:
   static const int NO_PARENT = -1;

   typedef QVector< championship::size_type >   id_index;
   typedef QSet< championship::size_type >      id_set;
   typedef QVector< id_index >                  tree_index;
   
   id_index    groupsIndex_m;
   tree_index  playersIndex_m;

   static std::vector< QString > createHeadersVector ();

   static void movePlayersFor1GroupDown ( int groupIndex_p, const id_index & groupsIds_cp,
      const championship::ids_list & idsForMove_cp, id_set & groupsForUpdate_p );

   static void movePlayersFor1GroupUp ( int groupIndex_p, const id_index & groupsIds_cp,
      const championship::ids_list & idsForMove_cp, id_set & groupsForUpdate_p );

   static void removePlayersFor1Group ( int groupIndex_p, const id_index & groupsIds_cp,
      const championship::ids_list & idsForDelete_cp, id_set & groupsForUpdate_p );

   void clearGroup ( const QModelIndex & groupIndex_cp );

   void fillPlayersFor1Group ( const championship::playersGroup_map & group_cp, id_index & groupIndex_rp );

   void operateSelectedIndexes ( const QModelIndexList & indexes_cp, 
      void ( * operation_p ) ( int, const id_index &, const championship::ids_list &, id_set & ) );

   void signalisedFillPlayersFor1Group ( const championship::playersGroup_map & group_cp, 
      const QModelIndex & groupIndex_cp );

   void updateIndexForGroupsSet ( const id_set & groupsForUpdate_cp );
}; // class QPlayersTreeModel 
#endif // QT_PLAYERS_TREE_MODEL_H_MRV