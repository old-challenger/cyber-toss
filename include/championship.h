#ifndef CHAMPIONSHIP_H_MRV
#define CHAMPIONSHIP_H_MRV

#include <QMap>
#include <QSet>
#include <QList>
#include <QVector>
#include <QString>

#include <cassert>

class championship {
public:
   enum { BAD_ID = -1, MIN_PLAYERS_FOR_TOSS = 4 };

   class player {
   public:
      player () : nickname_m(), clan_m() { ; }
                  
      player ( const QString & nickname_cp, const QString & clan_cp  ) : 
         nickname_m( nickname_cp ), clan_m( clan_cp ) { ; }

      player ( const player & etalon_cp ) : nickname_m( etalon_cp.nickname_m ), clan_m( etalon_cp.clan_m ) 
      { ; }

      const QString & nickname () const { return nickname_m; }

      const QString & clan () const { return clan_m; }
   private:
      QString  nickname_m,
               clan_m;
   }; // class player {

   typedef int                                  size_type;
   typedef QMap< size_type, player >            playersGroup_map;
   typedef QMap< size_type, playersGroup_map >  groups_map;
   typedef QVector< size_type >                 ids_vector;

   class round {
   public:
      class player_pair {
      public:
         player_pair () : groupId_m( BAD_ID ), playerId_m( BAD_ID ) { ; }

         player_pair ( size_type groupId_p, size_type playerId_p ) : groupId_m( groupId_p ), 
            playerId_m( playerId_p ) { ; }

         player_pair ( const player_pair & etalon_cp ) : groupId_m( etalon_cp.groupId_m ), 
            playerId_m( etalon_cp.playerId_m ) { ; }

         size_type groupId () const { return groupId_m; }
         size_type playerId () const { return playerId_m; }
      private:
         size_type groupId_m, playerId_m;
      }; // class player_pair {

      class game {
      public:
         enum result_enum {
            UNKNOWN        = 0,
            EXPECTED       = 1,
            UNEXPECTED     = 2,
            VALUES_AMOUNT  = 3
         };

         game () : favorite_m(), outsider_m(), favoriteNation_m( BAD_ID ), outsiderNation_m( BAD_ID ), 
            result_m( UNKNOWN ) { ; }

         game ( const player_pair & favorite_p, size_type favoritePlayesNation_p,
               const player_pair & outsider_p,  size_type outsiderPlayesNation_p, 
               result_enum result_p = UNKNOWN ) : 
            favorite_m( favorite_p ), outsider_m( outsider_p ), favoriteNation_m( favoritePlayesNation_p ), 
            outsiderNation_m( outsiderPlayesNation_p ), result_m( result_p ) 
         { ; }

         game ( const game & etalon_cp ) : 
            favorite_m( etalon_cp.favorite_m ), outsider_m( etalon_cp.outsider_m ), 
            favoriteNation_m( etalon_cp.favoriteNation_m ), outsiderNation_m( etalon_cp.outsiderNation_m ),
            result_m( etalon_cp.result_m ) 
         { ; }

         static const QString & resultTxt ( size_type resultId_p ) {
            assert( resultId_p < resultTxtValues().size() );
            return resultTxtValues().at( resultId_p );
         }

         static result_enum resultFromTxt ( const QString & resultTxt_cp ) {
            if ( resultTxtValues().at( UNKNOWN ) == resultTxt_cp )
               return UNKNOWN;
            else  if ( resultTxtValues().at( EXPECTED ) == resultTxt_cp )
               return EXPECTED;
            else  if ( resultTxtValues().at( UNEXPECTED ) == resultTxt_cp )
               return UNEXPECTED;
            return UNKNOWN;
         }

         const player_pair & favorite () const { return favorite_m; } 

         size_type favoriteNation () const { return favoriteNation_m; }

         const player_pair & looser () const { 
            assert( UNKNOWN != result_m );
            return EXPECTED == result_m ? outsider_m : favorite_m; 
         }

         const player_pair & outsider () const { return outsider_m; }

         size_type outsiderNation () const { return outsiderNation_m; }

         result_enum result () const { return result_m; }

         void setResult( result_enum result_p ) { result_m = result_p; }

         const player_pair & winner () const { 
            assert( UNKNOWN != result_m );
            return EXPECTED == result_m ? favorite_m : outsider_m; 
         }
      private:
         player_pair favorite_m, outsider_m;
         size_type favoriteNation_m, outsiderNation_m;
         result_enum result_m;

         static QVector< QString > createResultTxtValues () {
            QVector< QString > reslut_l( VALUES_AMOUNT );
            reslut_l[ UNKNOWN    ] = QLatin1String( "UNKNOWN"     );
            reslut_l[ EXPECTED   ] = QLatin1String( "EXPECTED"    );
            reslut_l[ UNEXPECTED ] = QLatin1String( "UNEXPECTED"  );
            return reslut_l;
         }

         static const QVector< QString > & resultTxtValues () {
            static const QVector< QString > values_scl = createResultTxtValues();
            return values_scl;
         }
      }; // class game {

      typedef QVector< game > games_vector;
      typedef QMap< size_type, ids_vector > roundsPlayers_map;

      round () : denominator_m( 0 ), players_m(), games_m() { ; }

      round ( size_type denominator_p ) : denominator_m( denominator_p ), players_m(), games_m() {  
         assert( denominator_m && ( 1 == denominator_m || !( denominator_m % 2 ) ) ); 
         games_m.reserve( denominator_p );
      }

      round ( const round & etalon_cp ) : denominator_m( etalon_cp.denominator_m ), 
         players_m( etalon_cp.players_m ), games_m( etalon_cp.games_m ) 
      {  assert( denominator_m && ( 1 == denominator_m || !( denominator_m % 2 ) ) ); }

      void addGames ( const round::games_vector & games_cp ) { games_m = games_cp; }

      void addPlayer ( const player_pair & player_cp ) {
         auto found_l = players_m.lowerBound( player_cp.groupId() );
         if ( players_m.end() == found_l || found_l.key() != player_cp.groupId() )
            found_l = players_m.insert( found_l, player_cp.groupId(), ids_vector() );
         assert( !found_l.value().contains( player_cp.playerId() ) );
         found_l.value().push_back( player_cp.playerId() );
      }

      template< class playersIdsContainer_type >
      void addPlayersGroup ( size_type groupId_p, const playersIdsContainer_type & playersIds_cp ) {
         assert( players_m.end() == players_m.find( groupId_p ) );
         auto newGroup_l = players_m.insert( groupId_p, ids_vector() );
         newGroup_l.value().reserve( playersIds_cp.size() );
         fillContainer( playersIds_cp, newGroup_l.value() );
      }

      void clearPlayers () { players_m.clear(); }

      size_type denominator () const { return denominator_m; }

      const games_vector & games () const { return games_m; }
      
      const roundsPlayers_map & players () const { return players_m; }

      size_type playersAmount () const { return doubleLayerSize( players_m ); }

      void removePlayer ( const player_pair & player_cp ) {
         auto found_l = players_m.find( player_cp.groupId() );
         assert( players_m.end() != found_l );
         assert( found_l.value().contains( player_cp.playerId() ) );
         found_l.value().remove( found_l.value().indexOf( player_cp.playerId() ) );
      }

      void setGameResult( size_type gameId_p, game::result_enum result_p ) {
         assert( games_m.size() > gameId_p );
         games_m[ gameId_p ].setResult( result_p );
      }

      const round & operator = ( const round & etalon_cp ) {
         assert( !denominator_m && etalon_cp.denominator_m );
         denominator_m = etalon_cp.denominator_m;
         assert( !( players_m.size() || games_m.size() ) );
         return *this;
      }
   private:
      size_type         denominator_m;
      roundsPlayers_map players_m;
      games_vector      games_m;
   }; // class round {

   typedef QMap< size_type, QString >           strings_map;
   typedef QList< size_type >                   ids_list;
   typedef QSet< size_type >                    ids_set;
   typedef QVector< round >                     rounds_vector;
   
   championship () : fileName_m(), players_m(), nations_m(), groupsNames_m(), nationsNames_m(), rounds_m(), 
      currentRound_m( 0 ) { 
      setDefaultValues(); 
   }

   void addGames ( const round::games_vector & games_cp ) {
      assert( !currentRound_m || games_cp.size() == rounds_m.at( currentRound_m ).denominator() );
      //players_m.clear();
      rounds_m[ currentRound_m ].addGames( games_cp );
   }

   void addNation ( size_type nationId_p ) { 
      nations_m.push_back( nationId_p );
   }

   size_type addPlayer ( size_type goupId_p, const QString & nickname_cp, const QString & clanname_cp ) {
      auto group_l = players_m.find( goupId_p );
      assert( players_m.end() != group_l );
      return smartMapInsert( player( nickname_cp, clanname_cp ), group_l.value() );
   }

   void clearRoundPlayers () {
      rounds_m[ currentRound_m ].clearPlayers();
   }

   size_type currentRound () const { return currentRound_m; }

   void deletePlayers ( size_type groupId_p, const ids_list & idsForDelete_cp ) {
      auto found_l = players_m.find( groupId_p );
      assert( players_m.end() != found_l );
      deletePlayers( idsForDelete_cp, found_l.value() );
   }

   void deleteNations ( const ids_set & idsForDelete_cp ) {
      for ( size_type i = 0; i < nations_m.size(); )
         if ( idsForDelete_cp.end() != idsForDelete_cp.find( nations_m.at( i ) ) )
            nations_m.erase( nations_m.begin() + i );
         else 
            ++i;
   }

   const QString & fileName () const { return fileName_m; }

   const strings_map & groupsNames () const  { return groupsNames_m; }

   bool loadDataFromFile ( const QString & fileName_cp );

   void movePlayersDown ( size_type goupId_p, const ids_list & idsForDelete_cp );

   void movePlayersUp ( size_type goupId_p, const ids_list & idsForDelete_cp );

   const ids_vector & nations () const { return nations_m; }

   const strings_map & nationsNames () const { return nationsNames_m; }

   bool nextRound () {  
      if ( !roundIsFinished() || rounds_m.size() - 1 == currentRound_m )
         return false;
      ++currentRound_m;
      return true;
   }

   const groups_map & players () const { return players_m; }

   size_type playersAmount () const { return doubleLayerSize( players_m ); }

   void reserveRounds ();

   const rounds_vector & rounds () const { return rounds_m; }

   bool roundIsFinished () const;

   bool saveDataToFile ( const QString & fileName_cp = QString() );

   void setDefaultValues ();

   void setGameResult ( size_type gameId_p, round::game::result_enum result_p );
private:
   QString        fileName_m;
   groups_map     players_m;
   ids_vector     nations_m;
   strings_map    groupsNames_m,
                  nationsNames_m;
   rounds_vector  rounds_m;
   size_type      currentRound_m;

   static void deletePlayers ( const ids_list & idsForDelete_cp, playersGroup_map & group_p ) {
      for ( auto idForDelete : idsForDelete_cp )
         group_p.erase( group_p.find( idForDelete ) );
   }

   template < class container_type >
   static typename container_type::size_type doubleLayerSize ( const container_type & conteyner_cp ) {
      typename container_type::size_type result_l = 0;
      for ( auto downLayerConteyner_it : conteyner_cp )
         result_l += downLayerConteyner_it.size();
      return result_l;
   } 

   static void insertPlayersInNearGroup (  const playersGroup_map & sourceGroup_p, 
         const ids_list & idsForDelete_cp, playersGroup_map & targetGroup_rp ) {
      for( auto id : idsForDelete_cp ) {
         assert( sourceGroup_p.end() != sourceGroup_p.find( id ) );
         smartMapInsert( sourceGroup_p[ id ], targetGroup_rp );
      }
   }

   QString makeIdsList ( const ids_vector & ids_cp );

   static void randomPlayersGroupSeparation ( const playersGroup_map & sourceGroup_cp, int firstPartSize_p,
      ids_vector & firstPart_rp, ids_vector & secondPart_rp );

   template< class data_type >
   static size_type smartMapInsert ( const data_type & value_cp, QMap< size_type, data_type > & map_rp ) {
      size_type currentKey_l = map_rp.size();
      playersGroup_map::const_iterator player_l;
      while( 1 ) {
         player_l = map_rp.lowerBound( currentKey_l );
         if ( map_rp.end() == player_l || player_l.key() != currentKey_l ) 
            break;
         --currentKey_l;
      }
      map_rp.insert( player_l, currentKey_l, value_cp );
      return currentKey_l;
   }
}; // class championship {

#endif // CHAMPIONSHIP_H_MRV